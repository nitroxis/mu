#ifndef REFERENCEPOINT_H
#define REFERENCEPOINT_H

#include <stdbool.h>
#include <stdatomic.h>
#include <pthread.h>
#include <semaphore.h>
#include <mpfr.h>
#include <mpc.h>

typedef struct
{
	unsigned int maxIterations;
	unsigned int numCoefficients;
	unsigned int approximatedIterations;
	unsigned int escapeTime;
	bool escaped;
	bool approximationAccurate;
	bool useApproximation;

	mpc_t c;
	mpc_t z;
	mpfr_t radius;
	mpfr_t radius4;
	
	mpc_t *coefficientsHeap;
	mpc_t *coefficients;
	mpc_t *coefficients2;
	mpc_t *coefficientsDerivatives;
	mpc_t *coefficientsDerivatives2;
	
	double *orbitDouble;
	double *coefficientsDouble;
	//double *coefficientsDerivativesDouble;
	
	pthread_t thread;
	sem_t started;
	atomic_bool stop;
	pthread_mutex_t approximationMutex;
	atomic_uint iterations;	
} referencepoint_t;

void referencepoint_init(referencepoint_t *ref, const unsigned int maxIterations, const unsigned int numCoefficients);
void referencepoint_destroy(referencepoint_t *ref);

void referencepoint_set(referencepoint_t *ref, const mpc_t c, const mpfr_t radius, const mpfr_prec_t precision);
void referencepoint_compute(referencepoint_t *ref);

void referencepoint_start(referencepoint_t *ref);
void referencepoint_wait(referencepoint_t *ref);
void referencepoint_stop(referencepoint_t *ref, bool wait);

const double *referencepoint_get_coefficients(referencepoint_t *ref, unsigned int *numApproximatedIterations);
const double *referencepoint_get_orbit(referencepoint_t *ref);
unsigned int referencepoint_get_current_iterations(const referencepoint_t *ref);
unsigned int referencepoint_get_escape_time(const referencepoint_t *ref);
bool referencepoint_is_escaped(const referencepoint_t *ref);

#endif