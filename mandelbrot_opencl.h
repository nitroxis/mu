#ifndef MANDELBROT_OPENCL_H
#define MANDELBROT_OPENCL_H

#include <CL/cl.h>
#include "mandelbrot.h"

mandelbrot_processor_t *mandelbrot_add_opencl_processor(mandelbrot_t *m, const cl_command_queue commandQueue, const unsigned int chunkSize, const unsigned int maxIterationsPerStep);

#endif