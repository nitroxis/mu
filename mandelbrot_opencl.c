#include <stdlib.h>
#include <stdint.h>
#include <CL/cl.h>

#include "clhelper.h"
#include "mandelbrot.h"

extern unsigned char mandelbrot_opencl_cl[];
extern unsigned int mandelbrot_opencl_cl_len;

typedef struct
{
	mandelbrot_t *mandelbrot;
	unsigned int chunkSize;
	unsigned int maxIterationsPerStep;
		
	cl_context context;
	cl_device_id deviceID;
	cl_command_queue commandQueue;
	
	cl_program program;
	
	cl_kernel initializeKernel;
	cl_kernel iterateKernel;
	
	size_t initializeKernelSize;
	size_t iterateKernelSize;
	
	cl_mem positionsBuffer;
	cl_mem stateBuffer;
	cl_mem iterationsBuffer;
	cl_mem coefficientsBuffer;
	cl_mem orbitBuffer;
} mandelbrot_opencl_t;

static void mandelbrot_opencl_destroy(mandelbrot_opencl_t *cl)
{
	E(clReleaseMemObject(cl->positionsBuffer));
	E(clReleaseMemObject(cl->stateBuffer));
	E(clReleaseMemObject(cl->iterationsBuffer));
	E(clReleaseMemObject(cl->coefficientsBuffer));
	E(clReleaseMemObject(cl->orbitBuffer));
	E(clReleaseKernel(cl->initializeKernel));
	E(clReleaseKernel(cl->iterateKernel));
	E(clReleaseProgram(cl->program));
}

static void mandelbrot_opencl_compute(mandelbrot_opencl_t *cl)
{
	mandelbrot_t *m = cl->mandelbrot;
	referencepoint_t *ref = mandelbrot_get_reference_point(m);
	
	const unsigned int maxIterations = mandelbrot_get_max_iterations(m);
	const unsigned int numCoefficients = mandelbrot_get_number_of_coefficients(m);
	const unsigned int width = mandelbrot_get_width(m);
	const unsigned int height = mandelbrot_get_height(m);
	const mpfr_prec_t precision = mandelbrot_get_precision(m);
	const double radiusInPixels = width > height ? height * 0.5 : width * 0.5; // radius in pixels.
	const double radiusInPixelsInv = 1.0 / radiusInPixels;
	
	// calculate the size of one pixel in the complex plane.
	mpfr_t temp;
	mpfr_init2(temp, precision);
	mpfr_div_d(temp, m->radius, radiusInPixels, MPFR_RNDN);
	const double pixelSize = mpfr_get_d(temp, MPFR_RNDN);
	mpfr_clear(temp);
	
	// get pixel offset, i.e. the difference between pixel (0,0) and the reference point.
	mpc_t temp2;
	mpc_init2(temp2, precision);
	mpc_sub(temp2, m->center, ref->c, MPFR_RNDN);
	mpfr_div_d(mpc_realref(temp2), mpc_realref(temp2), pixelSize, MPFR_RNDN);
	mpfr_sub_d(mpc_realref(temp2), mpc_realref(temp2), width * 0.5, MPFR_RNDN);
	mpfr_div_d(mpc_imagref(temp2), mpc_imagref(temp2), pixelSize, MPFR_RNDN);
	mpfr_sub_d(mpc_imagref(temp2), mpc_imagref(temp2), height * 0.5, MPFR_RNDN);
	const double offset[2] = { mpfr_get_d(mpc_realref(temp2), MPFR_RNDN), mpfr_get_d(mpc_imagref(temp2), MPFR_RNDN) };
	mpc_clear(temp2);	
	
	// get series approximation coefficients.
	unsigned int approximatedIterations;
	const double *coefficients = referencepoint_get_coefficients(ref, &approximatedIterations);
	
	// upload coefficients.
	E(clEnqueueWriteBuffer(cl->commandQueue, cl->coefficientsBuffer, CL_TRUE, 0, sizeof(double) * 2 * numCoefficients, coefficients, 0, NULL, NULL));
	
	// setup init kernel.
	E(clSetKernelArg(cl->initializeKernel, 4, sizeof(cl_uint),       &approximatedIterations));
	E(clSetKernelArg(cl->initializeKernel, 5, sizeof(cl_double) * 2, &offset[0]));
	E(clSetKernelArg(cl->initializeKernel, 6, sizeof(cl_double),     &pixelSize));
	E(clSetKernelArg(cl->initializeKernel, 7, sizeof(cl_double),     &radiusInPixelsInv));
		
	if(mandelbrot_stop_requested(m))
		return;
	
	chunk_t *chunk;
	while((chunk = mandelbrot_request_chunk(m, cl->chunkSize)) != NULL)
	{
		// set kernel args.
		E(clSetKernelArg(cl->initializeKernel, 8, sizeof(cl_uint), &chunk->count));
		E(clSetKernelArg(cl->iterateKernel, 4, sizeof(cl_uint), &chunk->count));

		size_t initializeSize = ((chunk->count + cl->initializeKernelSize - 1) / cl->initializeKernelSize) * cl->initializeKernelSize;
		size_t iterateSize = ((chunk->count + cl->iterateKernelSize - 1) / cl->iterateKernelSize) * cl->iterateKernelSize;
		
		// upload positions.
		cl_event positionsWritten;
		E(clEnqueueWriteBuffer(cl->commandQueue, cl->positionsBuffer, CL_FALSE, 0, sizeof(position_t) * chunk->count, chunk->positions, 0, NULL, &positionsWritten));
		
		cl_event initialized;
		E(clEnqueueNDRangeKernel(cl->commandQueue, cl->initializeKernel, 1, NULL, &initializeSize, &cl->initializeKernelSize, 1, &positionsWritten, &initialized));
		E(clReleaseEvent(positionsWritten));
		cl_event prevEvent = initialized;
		
		unsigned int currentIteration = approximatedIterations;
		while(currentIteration < maxIterations)
		{
			if(mandelbrot_stop_requested(m))
				break;

			unsigned int target = referencepoint_get_current_iterations(ref);
			unsigned int numIterations = target - currentIteration;

			if(numIterations > cl->maxIterationsPerStep)
			{
				numIterations = cl->maxIterationsPerStep;
			}	
			else if(numIterations <= 0 || (target != maxIterations && numIterations < 512))
			{
				continue;
			}

			cl_event orbitWritten;
			E(clEnqueueWriteBuffer(cl->commandQueue, cl->orbitBuffer, CL_FALSE, 0, numIterations * 2 * sizeof(double), &referencepoint_get_orbit(ref)[currentIteration * 2], 1, &prevEvent, &orbitWritten));
			E(clReleaseEvent(prevEvent));
			
			E(clSetKernelArg(cl->iterateKernel, 3, sizeof(cl_uint), &numIterations));
			E(clEnqueueNDRangeKernel(cl->commandQueue, cl->iterateKernel, 1, NULL, &iterateSize, &cl->iterateKernelSize, 1, &orbitWritten, &prevEvent));
			E(clReleaseEvent(orbitWritten));
			
			currentIteration += numIterations;

			E(clFinish(cl->commandQueue));
		}

		// read result and submit.
		E(clEnqueueReadBuffer(cl->commandQueue, cl->iterationsBuffer, CL_TRUE, 0, sizeof(cl_uint) * chunk->count, chunk->iterations, 1, &prevEvent, NULL));
		E(clReleaseEvent(prevEvent));
			
		mandelbrot_submit_chunk(m, chunk);
		
		if(mandelbrot_stop_requested(m))
			break;
	}
}

mandelbrot_processor_t *mandelbrot_add_opencl_processor(mandelbrot_t *m, const cl_command_queue commandQueue, const unsigned int chunkSize, const unsigned int maxIterationsPerStep)
{
	mandelbrot_opencl_t *cl = (mandelbrot_opencl_t *)malloc(sizeof(mandelbrot_opencl_t));
	
	cl->mandelbrot = m;
	cl->chunkSize = chunkSize;
	cl->maxIterationsPerStep = maxIterationsPerStep;
	
	// get info.
	cl->commandQueue = commandQueue;
	E(clGetCommandQueueInfo(commandQueue, CL_QUEUE_CONTEXT, sizeof(cl_context), &cl->context, NULL));
	E(clGetCommandQueueInfo(commandQueue, CL_QUEUE_DEVICE, sizeof(cl_device_id), &cl->deviceID, NULL));
	
	// compile program.
	printf("Creating program... "); fflush(stdout);
	const char *src[1] = { (char *)mandelbrot_opencl_cl };
	const size_t lengths[1] = { mandelbrot_opencl_cl_len };
	
	cl_int err;
	cl->program = clCreateProgramWithSource(cl->context, 1, src, lengths, &err); E(err);

	printf("done.\n"); fflush(stdout);
	
	printf("Building program... "); fflush(stdout);
	
	const unsigned int numCoefficients = mandelbrot_get_number_of_coefficients(m);
	char args[256];
	sprintf(args, "-cl-finite-math-only -cl-no-signed-zeros -DNUM_COEFFICIENTS=%u", numCoefficients);
	
	err = clBuildProgram(cl->program, 1, &cl->deviceID, args, NULL, NULL);
	
	{
		// print log.
		const size_t logSize = 1 << 16;
		char *log = (char *)malloc(logSize);
		
		log[0] = 0;
		
		E(clGetProgramBuildInfo(cl->program, cl->deviceID, CL_PROGRAM_BUILD_LOG, logSize, &log[0], NULL));
		printf("done.\n%s\n", log); fflush(stdout);
		
		free(log);
	}
	
	E(err);	
	
	// create kernels.
	printf("Creating kernels... "); fflush(stdout);
	cl->initializeKernel   = clCreateKernel(cl->program, "initialize", &err); E(err);
	cl->iterateKernel      = clCreateKernel(cl->program, "iterate",    &err); E(err);
	
	E(clGetKernelWorkGroupInfo(cl->initializeKernel, cl->deviceID, CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), &cl->initializeKernelSize, NULL));
	E(clGetKernelWorkGroupInfo(cl->initializeKernel, cl->deviceID, CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), &cl->iterateKernelSize, NULL));
	printf("done.\n"); fflush(stdout);
	
	// create buffers.
	printf("Creating buffers... "); fflush(stdout);
	cl->positionsBuffer    = clCreateBuffer(cl->context, CL_MEM_READ_ONLY,  sizeof(position_t) * chunkSize,               NULL, &err); E(err);
	cl->stateBuffer        = clCreateBuffer(cl->context, CL_MEM_READ_WRITE, sizeof(cl_double) * 4 * chunkSize,            NULL, &err); E(err);
	cl->iterationsBuffer   = clCreateBuffer(cl->context, CL_MEM_READ_WRITE, sizeof(cl_uint) * chunkSize,                  NULL, &err); E(err);
	cl->coefficientsBuffer = clCreateBuffer(cl->context, CL_MEM_READ_ONLY,  sizeof(cl_double) * 2 * numCoefficients,      NULL, &err); E(err);
	cl->orbitBuffer        = clCreateBuffer(cl->context, CL_MEM_READ_ONLY,  sizeof(cl_double) * 2 * maxIterationsPerStep, NULL, &err); E(err);
	printf("done.\n"); fflush(stdout);
	
	// set args.
	E(clSetKernelArg(cl->initializeKernel, 0, sizeof(cl_mem), &cl->positionsBuffer));
	E(clSetKernelArg(cl->initializeKernel, 1, sizeof(cl_mem), &cl->stateBuffer));
	E(clSetKernelArg(cl->initializeKernel, 2, sizeof(cl_mem), &cl->iterationsBuffer));
	E(clSetKernelArg(cl->initializeKernel, 3, sizeof(cl_mem), &cl->coefficientsBuffer));
	
	E(clSetKernelArg(cl->iterateKernel,    0, sizeof(cl_mem), &cl->stateBuffer));
	E(clSetKernelArg(cl->iterateKernel,    1, sizeof(cl_mem), &cl->iterationsBuffer));
	E(clSetKernelArg(cl->iterateKernel,    2, sizeof(cl_mem), &cl->orbitBuffer));
	
	return mandelbrot_add_processor(m, cl, (mandelbrot_processor_func_t)mandelbrot_opencl_compute, (mandelbrot_processor_func_t)mandelbrot_opencl_destroy);
}

