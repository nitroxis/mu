#include "stopwatch.h"

#if defined(_WIN32) || defined(_WIN64)
#include <Windows.h>

static LARGE_INTEGER freq;
static int freqInitialized = 0;

double get_time()
{
	if(!freqInitialized)
	{
		if(QueryPerformanceFrequency(&freq))
		{
			freqInitialized = 1;
		}
		else
		{
			return 0.0;
		}
	}
	
	LARGE_INTEGER time;
	if(QueryPerformanceCounter(&time))
	{
		return (double)time.QuadPart / freq.QuadPart;
	}
	
	return 0.0;
}

#else
#include <time.h>
#include <sys/time.h>

double get_time()
{
	struct timeval time;
	if (gettimeofday(&time, NULL))
		return 0.0;
	
	return (double)time.tv_sec + (double)time.tv_usec * .000001;
}

#endif

