#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <mpfr.h>
#include <mpc.h>
#include <assert.h>

#include "mandelbrot.h"
#include "clhelper.h"
#include "referencepoint.h"
#include "glitchanalyzer.h"
#include "atom.h"
#include "stopwatch.h"

struct mandelbrot_processor
{
	mandelbrot_t *mandelbrot;
	void *data;

	mandelbrot_processor_func_t compute;
	mandelbrot_processor_func_t destroy;

	mandelbrot_processor_t *next;
};

static const unsigned char gradientData[] =
{
	0x00, 0x00, 0x30,
	0x08, 0x08, 0x64,
	0x20, 0x6B, 0xCB,
	0xED, 0xFF, 0xFF,
	0xFF, 0x99, 0x00,
	0x70, 0x10, 0x20
};

void mandelbrot_init(mandelbrot_t *m, const unsigned int numCoefficients, const unsigned int width, const unsigned int height)
{
	m->numCoefficients = numCoefficients;
	m->maxIterations = 0;
	m->iterations = NULL;
	m->colors = NULL;
	m->positions = NULL;
	m->positionsNext = NULL;

	m->gradient = &gradientData[0];
	m->gradientSize = 6;

	m->precision = 53;
	mpc_init2(m->center, m->precision);
	mpfr_init2(m->radius, m->precision);
	m->findGoodReference = true;
	m->referencePoint = (referencepoint_t *)malloc(sizeof(referencepoint_t));

	m->numProcessors = 0;
	m->processors = NULL;

	m->firstChunk = m->lastChunk = NULL;
	pthread_mutex_init(&m->chunkQueueLock, NULL);

	m->updateCallback = NULL;
	m->updateParameter = NULL;

	m->thread = 0;
	sem_init(&m->started, 0, 0);
	atomic_store(&m->stop, false);

	m->glitchAnalyzer = NULL;

	mandelbrot_set_size(m, width, height);
	mandelbrot_reset(m);
}

void mandelbrot_glitch_init(mandelbrot_t *m, const cl_command_queue commandQueue)
{
	m->glitchAnalyzer = (glitchanalyzer_t *)malloc(sizeof(glitchanalyzer_t));
	glitchanalyzer_init(m->glitchAnalyzer, commandQueue, m->width, m->height);
}

void mandelbrot_destroy(mandelbrot_t *m)
{
	assert(m->thread == 0);

	while(m->processors != NULL)
		mandelbrot_remove_processor(m, m->processors);

	if(m->iterations != NULL) free(m->iterations);
	if(m->positions != NULL) free(m->positions);
	if(m->positionsNext != NULL) free(m->positionsNext);
	if(m->colors != NULL) free(m->colors);

	mpc_clear(m->center);
	mpfr_clear(m->radius);
	referencepoint_destroy(m->referencePoint);
	free(m->referencePoint);

	sem_destroy(&m->started);

	pthread_mutex_destroy(&m->chunkQueueLock);

	if(m->glitchAnalyzer != NULL)
		glitchanalyzer_destroy(m->glitchAnalyzer);
}

mandelbrot_processor_t *mandelbrot_add_processor(mandelbrot_t *m, void *data, const mandelbrot_processor_func_t compute, const mandelbrot_processor_func_t destroy)
{
	mandelbrot_processor_t *processor = (mandelbrot_processor_t *)malloc(sizeof(mandelbrot_processor_t));
	processor->mandelbrot = m;
	processor->data = data;
	processor->compute = compute;
	processor->destroy = destroy;
	processor->next = NULL;

	if(m->processors == NULL)
	{
		m->processors = processor;
	}
	else
	{
		mandelbrot_processor_t *current = m->processors;

		while(current->next != NULL)
		{
			current = current->next;
		}

		current->next = processor;
	}

	++m->numProcessors;

	return processor;
}

int mandelbrot_remove_processor(mandelbrot_t *m, mandelbrot_processor_t *processor)
{
	assert(processor->mandelbrot == m);

	// remove from list.
	if(m->processors == processor)
	{
		m->processors = processor->next;
	}
	else
	{
		mandelbrot_processor_t *current = m->processors;

		while(current->next != processor)
		{
			current = current->next;
			if(current == NULL)
				return 0;
		}

		current->next = processor->next;
	}

	processor->destroy(processor->data);
	free(processor);

	--m->numProcessors;

	return 1;
}

chunk_t *mandelbrot_request_chunk(mandelbrot_t *m, const unsigned int maxCount)
{
	assert(m != NULL);

	if(mandelbrot_stop_requested(m) || maxCount == 0)
		return NULL;

	const unsigned int offset = atomic_fetch_add(&m->offset, maxCount);
	if(offset > m->count)
		return NULL;

	unsigned int count = m->count - offset;
	if(count > maxCount)
		count = maxCount;

	if(count == 0)
		return NULL;

	chunk_t *chunk = (chunk_t *)malloc(sizeof(chunk_t));
	chunk->count = count;
	chunk->positions = &m->positions[offset];
	chunk->iterations = (uint32_t *)malloc(sizeof(uint32_t) * count);
	chunk->next = NULL;

	return chunk;
}

static void mandelbrot_destroy_chunk(chunk_t *chunk)
{
	assert(chunk != NULL);
	assert(chunk->iterations != NULL);

	free(chunk->iterations);
	chunk->iterations = NULL;

	free(chunk);
}

// gradient interpolation.
static inline uint32_t interpolate(const unsigned char *gradientColors, const unsigned int length, const double t)
{
	unsigned int i0 = (int)t;
	unsigned int i1 = i0 + 1;
	const double f1 = t - i0;
	const double f0 = 1.0 - f1;

	i0 = (i0 % length) * 3;
	i1 = (i1 % length) * 3;

	return (uint32_t)(gradientColors[i0 + 0] * f0 + gradientColors[i1 + 0] * f1) << 16
	     | (uint32_t)(gradientColors[i0 + 1] * f0 + gradientColors[i1 + 1] * f1) << 8
	     | (uint32_t)(gradientColors[i0 + 2] * f0 + gradientColors[i1 + 2] * f1);
}

static void mandelbrot_colorize(mandelbrot_t *m, const position_t *positions, const unsigned int count)
{
	for(unsigned int i = 0; i < count; i++)
	{
		const unsigned int offset = positions[i].y * m->width + positions[i].x;
		const unsigned int n = m->iterations[offset];
		uint32_t *color = &m->colors[offset];

		if(n == GLITCHED)
		{
			*color = 0x333333;
		}
		else if(n & ESCAPED_BIT)
		{
			*color = interpolate(m->gradient, m->gradientSize, 0.01 * (n & (~ESCAPED_BIT)));
		}
		else
		{
			*color = 0;
		}
	}
}

static void mandelbrot_process(mandelbrot_t *m, const position_t *positions, const uint32_t *iterations, const unsigned int count)
{
	assert(m != NULL);
	assert(positions != NULL);
	assert(iterations != NULL);

	for(unsigned int i = 0; i < count; i++)
	{
		const position_t p = positions[i];
		const unsigned int offset = p.y * m->width + p.x;
		const uint32_t n = iterations[i];

		m->iterations[offset] = n;
		if(n == GLITCHED)
		{
			glitchanalyzer_set(m->glitchAnalyzer, p.x, p.y);
			m->positionsNext[m->numGlitches++] = p;
		}
	}


	m->processedPixels += count;

	mandelbrot_colorize(m, positions, count);
}

void mandelbrot_submit_chunk(mandelbrot_t *m, chunk_t *chunk)
{
	chunk->next = NULL;

	pthread_mutex_lock(&m->chunkQueueLock);

	if(m->lastChunk == NULL)
	{
		m->firstChunk = chunk;
		m->lastChunk = chunk;
	}
	else
	{
		m->lastChunk->next = chunk;
		m->lastChunk = chunk;
	}

	pthread_mutex_unlock(&m->chunkQueueLock);
}

unsigned int mandelbrot_get_max_iterations(const mandelbrot_t *m)
{
	return m->maxIterations;
}

void mandelbrot_set_max_iterations(mandelbrot_t *m, const unsigned int maxIterations)
{
	if(m->maxIterations > 0)
		referencepoint_destroy(m->referencePoint);

	m->maxIterations = maxIterations;
	referencepoint_init(m->referencePoint, maxIterations, m->numCoefficients);
}

unsigned int mandelbrot_get_number_of_coefficients(const mandelbrot_t *m)
{
	return m->numCoefficients;
}

unsigned int mandelbrot_get_width(const mandelbrot_t *m)
{
	return m->width;
}

unsigned int mandelbrot_get_height(const mandelbrot_t *m)
{
	return m->height;
}

void mandelbrot_set_size(mandelbrot_t *m, const unsigned int width, const unsigned int height)
{
	m->width = width;
	m->height = height;

	m->iterations      = (uint32_t *)realloc(m->iterations,    sizeof(uint32_t)   * width * height);
	m->positions     = (position_t *)realloc(m->positions,     sizeof(position_t) * width * height);
	m->positionsNext = (position_t *)realloc(m->positionsNext, sizeof(position_t) * width * height);
	m->colors          = (uint32_t *)realloc(m->colors,        sizeof(uint32_t)   * width * height);

	if(m->glitchAnalyzer != NULL)
	{
		const cl_command_queue commandQueue = m->glitchAnalyzer->commandQueue;
		glitchanalyzer_destroy(m->glitchAnalyzer);
		glitchanalyzer_init(m->glitchAnalyzer, commandQueue, width, height);
	}
}

const uint32_t *mandelbrot_get_iterations(const mandelbrot_t *m)
{
	return m->iterations;
}

const uint32_t *mandelbrot_get_colors(const mandelbrot_t *m)
{
	return m->colors;
}

mpfr_prec_t mandelbrot_get_precision(const mandelbrot_t *m)
{
	return m->precision;
}

void mandelbrot_set_precision(mandelbrot_t *m, const mpfr_prec_t precision)
{
	mpfr_t temp;
	mpfr_init2(temp, precision);

	mpfr_set(temp, mpc_realref(m->center), MPFR_RNDN);
	mpfr_set_prec(mpc_realref(m->center), precision);
	mpfr_set(mpc_realref(m->center), temp, MPFR_RNDN);

	mpfr_set(temp, mpc_imagref(m->center), MPFR_RNDN);
	mpfr_set_prec(mpc_imagref(m->center), precision);
	mpfr_set(mpc_imagref(m->center), temp, MPFR_RNDN);

	mpfr_set(temp, m->radius, MPFR_RNDN);
	mpfr_set_prec(m->radius, precision);
	mpfr_set(m->radius, temp, MPFR_RNDN);

	mpfr_clear(temp);
	m->precision = precision;
}

void mandelbrot_auto_precision(mandelbrot_t *m)
{
	const int optimal = 128;
	const int threshold = 32;

	long exponent = -(long)mpfr_get_exp(m->radius);

	if(exponent < 0)
		exponent = 0;

	long diff = exponent - (long)m->precision;
	if(diff >= -(optimal - threshold) || diff < -(optimal + threshold))
	{
		mpfr_prec_t newPrec = ((exponent + optimal) / mp_bits_per_limb) * mp_bits_per_limb;
		if(newPrec < mp_bits_per_limb)
			newPrec = mp_bits_per_limb;

		if(newPrec != m->precision)
		{
			mandelbrot_set_precision(m, newPrec);
		}
	}
}

referencepoint_t *mandelbrot_get_reference_point(mandelbrot_t *m)
{
	return m->referencePoint;
}

bool mandelbrot_get_use_approximation(const mandelbrot_t *m)
{
	return m->referencePoint->useApproximation;
}

void mandelbrot_set_use_approximation(mandelbrot_t *m, const bool useApproximation)
{
	m->referencePoint->useApproximation = useApproximation;
}

void mandelbrot_from_string(mandelbrot_t *m, const char *real, const char *imag, const char *radius)
{
	if(radius != NULL)
	{
		mpfr_strtofr(m->radius, radius, NULL, 10, MPFR_RNDN);
		mandelbrot_auto_precision(m);
		mpfr_strtofr(m->radius, radius, NULL, 10, MPFR_RNDN);
	}

	if(real != NULL)
	{
		mpfr_strtofr(mpc_realref(m->center), real, NULL, 10, MPFR_RNDN);
	}

	if(imag != NULL)
	{
		mpfr_strtofr(mpc_imagref(m->center), imag, NULL, 10, MPFR_RNDN);
	}
}

static int find_variable(const char *str, const char *name, char **value)
{
	const size_t nameLength = strlen(name);
	char *pre = malloc(nameLength + 2);
	strcpy(pre, name);
	pre[nameLength] = '=';
	pre[nameLength + 1] = 0;

	// find "name="
	*value = strstr(str, pre);
	free(pre);

	if(*value == NULL)
		return 0;

	// go to value.
	*value += nameLength + 1;

	// get length of value.
	size_t length = strlen(*value);

	// is there a newline before the end?
	char *delimiter = strpbrk(*value, " \t\r\n");
	if(delimiter != NULL)
		length = delimiter - *value;

	return (int)length;
}

void mandelbrot_parse_sft(mandelbrot_t *m, const char *str)
{
	char *real;
	char *imag;
	char *radius;

	find_variable(str, "r", &real);
	find_variable(str, "i", &imag);
	find_variable(str, "s", &radius);

	mandelbrot_from_string(m, real, imag, radius);

	char *value;
	if(find_variable(str, "iteration_limit", &value))
	{
		unsigned int v = strtol(value, NULL, 10);
		if(v > 0)
			mandelbrot_set_max_iterations(m, v);
	}
}

void mandelbrot_reset(mandelbrot_t *m)
{
	m->precision = 53;
	mpc_set_prec(m->center, m->precision);
	mpfr_set_prec(m->radius, m->precision);
	mpc_set_ui(m->center, 0, MPFR_RNDN);
	mpfr_set_ui(m->radius, 2, MPFR_RNDN);

	mandelbrot_set_max_iterations(m, 1024);
}

void mandelbrot_move(mandelbrot_t *m, const double x, const double y)
{
	mpc_t d;
	mpc_init2(d, m->precision);
	mpc_set_d_d(d, x, y, MPFR_RNDN);
	mpc_mul_fr(d, d, m->radius, MPFR_RNDN);
	mpc_add(m->center, m->center, d, MPFR_RNDN);
	mpc_clear(d);
}

void mandelbrot_zoom(mandelbrot_t *m, const double x, const double y, const double zoomFactor)
{
	const double invFactor = 1.0 - zoomFactor;
	mandelbrot_move(m, x * invFactor, y * invFactor);
	mpfr_mul_d(m->radius, m->radius, zoomFactor, MPFR_RNDN);
}

static void *processor_stub(void *arg)
{
	mandelbrot_processor_t *processor = (mandelbrot_processor_t *)arg;
	processor->compute(processor->data);
	return NULL;
}

void mandelbrot_compute(mandelbrot_t *m)
{
	if(mandelbrot_stop_requested(m))
		return;

	printf("Max. Iterations=%u, MPFR precision=%u", m->maxIterations, (unsigned int)m->precision);
	referencepoint_set(m->referencePoint, m->center, m->radius, m->precision);

	// find good reference point.
	if(m->findGoodReference)
	{
		unsigned int period = find_period(m->center, m->radius, m->maxIterations);
		printf(", Period="); fflush(stdout);
		if(period)
		{
			printf("%u", period);
			if(mandelbrot_stop_requested(m))
			{
				printf("\n");
				return;
			}

			printf(", Searching for atom..."); fflush(stdout);
			atom_t atom;
			if(find_atom(&atom, m->center, m->radius, period))
			{
				mpc_t diff;
				mpc_init2(diff, m->precision);
				mpc_sub(diff, atom.nucleus, m->center, MPFR_RNDN);

				mpfr_t dist;
				mpfr_init2(dist, m->precision);
				mpc_abs(dist, diff, MPFR_RNDN);
				mpc_clear(diff);

				mpfr_div(dist, dist, m->radius, MPFR_RNDN);
				const double distD = mpfr_get_d(dist, MPFR_RNDN);
				printf(" %f", distD);

				if(distD > 100.0)
				{
					printf(" too far off");
				}
				else
				{
					referencepoint_set(m->referencePoint, atom.nucleus, m->radius, m->precision);
					printf(" done");
				}

				mpc_clear(atom.nucleus);
				mpfr_clear(atom.radius);
			}
			else
			{
				printf(" failed");
			}
		}
		else
		{
			printf("?");
		}
	}

	fflush(stdout);

	if(mandelbrot_stop_requested(m))
	{
		printf("\n");
		return;
	}

	// start reference point computation.
	bool storeReferencePointIterations = false;
	unsigned int refX = 0;
	unsigned int refY = 0;

	referencepoint_start(m->referencePoint);

	// initial positions.
	m->count = m->width * m->height;
	unsigned int posIndex = 0;
	for(unsigned int y = 0; y < m->height; ++y)
	{
		unsigned int row;

		if(y & 1)
			row = m->height / 2 - y / 2 - 1;
		else
			row = m->height / 2 + y / 2;

		for(unsigned int x = 0; x < m->width; ++x)
		{
			m->positions[posIndex].x = x;
			m->positions[posIndex].y = row;
			++posIndex;
		}
	}

	const unsigned int numProcessors = m->numProcessors;
	pthread_t threads[numProcessors];

	printf(". Iterating..."); fflush(stdout);

	// main loop.
	while(m->count > 0)
	{
		printf(" %u", m->count); fflush(stdout);

		if(mandelbrot_stop_requested(m))
			break;

		if(m->glitchAnalyzer != NULL)
			glitchanalyzer_clear(m->glitchAnalyzer);

		atomic_store(&m->offset, 0);
		m->numGlitches = 0;
		m->processedPixels = 0;

		// start processors.
		unsigned int i = 0;
		for(mandelbrot_processor_t *p = m->processors; p != NULL; p = p->next, ++i)
		{
			pthread_create(&threads[i], NULL, processor_stub, p);
		}

		// process incoming chunks.
		while(m->processedPixels < m->count && !mandelbrot_stop_requested(m))
		{
			chunk_t *chunk = NULL;

			pthread_mutex_lock(&m->chunkQueueLock);

			// dequeue.
			chunk = m->firstChunk;
			if(chunk != NULL)
			{
				m->firstChunk = chunk->next;
				if(m->lastChunk == chunk)
					m->lastChunk = NULL;
			}

			pthread_mutex_unlock(&m->chunkQueueLock);

			if(chunk != NULL)
			{
				mandelbrot_process(m, chunk->positions, chunk->iterations, chunk->count);
				mandelbrot_destroy_chunk(chunk);
			}
		}

		// wait for each thread to finish.
		i = 0;
		for(mandelbrot_processor_t *p = m->processors; p != NULL; p = p->next, ++i)
		{
			pthread_join(threads[i], NULL);
		}

		// discard remaining chunks.
		while(m->firstChunk != NULL)
		{
			mandelbrot_destroy_chunk(m->firstChunk);
			m->firstChunk = m->firstChunk->next;
		}
		m->lastChunk = NULL;

		referencepoint_wait(m->referencePoint);

		if(storeReferencePointIterations)
		{
			position_t refP = { refX, refY };
			uint32_t iterations = 0;

			if(referencepoint_is_escaped(m->referencePoint))
			{
				iterations = ESCAPED_BIT | referencepoint_get_escape_time(m->referencePoint);
			}

			mandelbrot_process(m, &refP, &iterations, 1);
		}

		if(mandelbrot_stop_requested(m))
			break;

		if(m->glitchAnalyzer != NULL)
		{
			if(m->numGlitches == 0)
				break;

			// set new count.
			m->count = m->numGlitches;

			// find blob.
			// TODO: this is not worth it for really small glitches.
			glitchanalyzer_find_blob(m->glitchAnalyzer, &refX, &refY, &m->stop);

			if(mandelbrot_stop_requested(m))
				break;

			// compute new reference point.
			const double radiusInPixelsInv = m->width > m->height ? 2.0 / m->height : 2.0 / m->width;
			const double blobX = (refX - (m->width * 0.5)) * radiusInPixelsInv;
			const double blobY = (refY - (m->height * 0.5)) * radiusInPixelsInv;
			mpc_t glitchC;
			mpc_init2(glitchC, m->precision);
			mpc_set_d_d(glitchC, blobX, blobY, MPFR_RNDN);
			mpc_mul_fr(glitchC, glitchC, m->radius, MPFR_RNDN);
			mpc_add(glitchC, glitchC, m->center, MPFR_RNDN);

			referencepoint_set(m->referencePoint, glitchC, m->radius, m->precision);
			mpc_clear(glitchC);

			// start new reference point.
			referencepoint_start(m->referencePoint);

			// swap position buffers.
			position_t *backup = m->positions;
			m->positions = m->positionsNext;
			m->positionsNext = backup;

			// exlude reference point position from calculation.
			for(unsigned int i = 0; i < m->numGlitches; i++)
			{
				if(m->positions[i].x == refX && m->positions[i].y == refY)
				{
					m->positions[i] = m->positions[m->count - 1];
					--m->count;
				}
			}

			storeReferencePointIterations = true;
		}
		else
		{
			break;
		}
	}

	referencepoint_stop(m->referencePoint, true);
	printf(" done.\n"); fflush(stdout);
}

static void *stub(void *arg)
{
	mandelbrot_t *m = (mandelbrot_t *)arg;
	sem_post(&m->started);
	mandelbrot_compute(m);
	return NULL;
}

void mandelbrot_start(mandelbrot_t *m)
{
	atomic_store(&m->stop, false);
	pthread_create(&m->thread, NULL, stub, m);
	sem_wait(&m->started);
}

void mandelbrot_wait(mandelbrot_t *m)
{
	if(m->thread == 0)
		return;

	pthread_join(m->thread, NULL);
	m->thread = 0;
}

void mandelbrot_stop(mandelbrot_t *m, bool wait)
{
	atomic_store(&m->stop, true);
	referencepoint_stop(m->referencePoint, false);

	if(wait)
		mandelbrot_wait(m);
}

bool mandelbrot_stop_requested(const mandelbrot_t *m)
{
	return atomic_load(&m->stop);
}
