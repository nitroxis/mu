#include <SDL2/SDL.h>
#include <math.h>

#include "mandelbrot.h"

void gui_main(mandelbrot_t *mandelbrot)
{
	// Initialize SDL.
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "Failed to initialize SDL.\n");
		return;
	}		
	
	SDL_Window *window = SDL_CreateWindow("Mandelbrot with perturbation and series approximation", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, mandelbrot_get_width(mandelbrot), mandelbrot_get_height(mandelbrot), SDL_WINDOW_RESIZABLE);
	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
	SDL_Texture *texture = NULL;
		
	int mouseX = 0, mouseY = 0;
	bool run = true;
	while(run)
	{
		// event handling
		SDL_Event e;
		while(SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
			{
				run = false;
			}
			else if (e.type == SDL_KEYDOWN)
			{
				switch(e.key.keysym.sym)
				{
					case SDLK_ESCAPE:
						run = false;
						break;
					
					case SDLK_SPACE:
						mandelbrot_stop(mandelbrot, true);
						mandelbrot_reset(mandelbrot);
						mandelbrot_start(mandelbrot);
						break;
					
					case SDLK_KP_PLUS:
						mandelbrot_stop(mandelbrot, true);
						mandelbrot_set_max_iterations(mandelbrot, mandelbrot_get_max_iterations(mandelbrot) * 2);
						mandelbrot_start(mandelbrot);
						break;
					
					case SDLK_KP_MINUS:
						mandelbrot_stop(mandelbrot, true);
						unsigned int max = mandelbrot_get_max_iterations(mandelbrot) / 2;
						if(max < 1)
							max = 1;
					
						mandelbrot_set_max_iterations(mandelbrot, max);
						mandelbrot_start(mandelbrot);
						break;
						
					case SDLK_r:
						mandelbrot_stop(mandelbrot, true);
						mandelbrot->findGoodReference = !mandelbrot->findGoodReference;
						printf("Reference atom search: %s\n", mandelbrot->findGoodReference ? "on" : "off");
						mandelbrot_start(mandelbrot);
						break;
						
					case SDLK_a:						
						mandelbrot_stop(mandelbrot, true);
						bool useApprox = !mandelbrot_get_use_approximation(mandelbrot);
						mandelbrot_set_use_approximation(mandelbrot, useApprox);
						printf("Series approximation: %s\n", useApprox ? "on" : "off");
						mandelbrot_start(mandelbrot);
						break;
				}
			}
			else if (e.type == SDL_WINDOWEVENT)
			{
				if(e.window.event == SDL_WINDOWEVENT_RESIZED)
				{
					mandelbrot_stop(mandelbrot, true);
					mandelbrot_set_size(mandelbrot, e.window.data1, e.window.data2);
				
					if(texture != NULL)
					{
						SDL_DestroyTexture(texture);
						texture = NULL;
					}
					
					printf("Size: %u x %u\n", e.window.data1, e.window.data2);
					mandelbrot_start(mandelbrot);
				}
			}
			else if (e.type == SDL_MOUSEBUTTONDOWN)
			{
				if((e.button.button & SDL_BUTTON_LMASK) && e.button.clicks == 2)
				{
					const unsigned int width = mandelbrot_get_width(mandelbrot);
					const unsigned int height = mandelbrot_get_height(mandelbrot);
					const double factor = 2.0 / (width > height ? height : width);

					const double x = ((double)mouseX - width * 0.5) * factor;
					const double y = ((double)mouseY - height * 0.5) * factor;
					mandelbrot_stop(mandelbrot, true);
					mandelbrot_move(mandelbrot, x, y);
					mandelbrot_start(mandelbrot);
				}
			}
			else if (e.type == SDL_MOUSEMOTION)
			{
				mouseX = e.motion.x;
				mouseY = e.motion.y;

				if(e.motion.state & SDL_BUTTON_LMASK)
				{
					const unsigned int width = mandelbrot_get_width(mandelbrot);
					const unsigned int height = mandelbrot_get_height(mandelbrot);
					const double factor = 2.0 / (width > height ? height : width);

					const double x = e.motion.xrel * factor;
					const double y = e.motion.yrel * factor;
					mandelbrot_stop(mandelbrot, true);
					mandelbrot_move(mandelbrot, -x, -y);
					mandelbrot_start(mandelbrot);
				}
			}
			else if (e.type == SDL_MOUSEWHEEL)
			{
				const unsigned int width = mandelbrot_get_width(mandelbrot);
				const unsigned int height = mandelbrot_get_height(mandelbrot);
				const double factor = 2.0 / (width > height ? height : width);

				const double x = ((double)mouseX - width * 0.5) * factor;
				const double y = ((double)mouseY - height * 0.5) * factor;
				const double zoom = pow(0.5, e.wheel.y);
				mandelbrot_stop(mandelbrot, true);
				mandelbrot_zoom(mandelbrot, x, y, zoom);
				mandelbrot_auto_precision(mandelbrot);
				mandelbrot_start(mandelbrot);
			}
		}
		
		const SDL_Rect textureRect = { 0, 0, mandelbrot_get_width(mandelbrot), mandelbrot_get_height(mandelbrot) };
		
		if(texture == NULL)
			texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, textureRect.w, textureRect.h);
		SDL_UpdateTexture(texture, &textureRect, mandelbrot_get_colors(mandelbrot), textureRect.w * 4);
		
		SDL_RenderClear(renderer);
		SDL_RenderCopy(renderer, texture, NULL, &textureRect);
		SDL_RenderPresent(renderer);
		
		SDL_Delay(30);
	}
	
	if(texture != NULL)
		SDL_DestroyTexture(texture);
		
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
}
