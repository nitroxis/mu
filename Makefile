CLSOURCES = mandelbrot_opencl.cl glitchanalyzer.cl
SOURCES = main.c mandelbrot.c referencepoint.c atom.c mandelbrot_opencl.c glitchanalyzer.c stopwatch.c
EXECUTABLE = mu
CC = gcc
CFLAGS = -O3 -std=c99 -Wall -Wextra -Wpedantic -mtune=native -march=native -fno-ident
LDFLAGS = -lpthread -lm -lmpc -lmpfr -lgmp -lSDL2 -lOpenCL

SYSTEM ?= unix
include Makefile.$(SYSTEM)



CLSOURCES_C = $(CLSOURCES:.cl=_cl.c)
SOURCES += $(CLSOURCES_C)
OBJECTS = $(SOURCES:.c=.o)



all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@

%_cl.c: %.cl
	xxd -i $< $@

clean:
	rm -f $(OBJECTS) $(EXECUTABLE)