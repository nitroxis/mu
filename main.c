#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <getopt.h>
#include <CL/cl.h>

#include "mandelbrot.h"
#include "mandelbrot_opencl.h"
#include "clhelper.h"

#ifndef NOGUI
void gui_main(mandelbrot_t *m);
#endif

typedef struct
{
	unsigned int platformIndex;
	unsigned int deviceIndex;
	
	cl_context context;
	cl_command_queue commandQueue;
} context_and_queue_t;

static void print_usage()
{
	printf("Options:\n");
	printf("-? --help                  Show this help text.\n");
	printf("-d --device         <x:y>  Use device x of platform y. You can use multiple devices to speed up rendering.\n");
	printf("-w --width          <x>    Sets image width to x.\n");
	printf("-h --height         <y>    Sets image height to y.\n");
	printf("-n --no-gui                Don't show an interactive user interface. The program will render one image and then exit.\n");
	printf("-r --real           <val>  Sets the real part of the center coordinate to val.\n");
	printf("-i --imag           <val>  Sets the imaginary part of the center coordinate to val.\n");
	printf("-s --radius         <val>  Sets the radius of the visibile region to val.\n");
	printf("-l --max-iterations <n>    Sets the maximum number of iterations to n.\n");
	printf("-o --out            <name> Sets the output file name. The image will be saved as Portable Pixel Map (ppm) file.\n");
	
}

static void print_opencl_info()
{
	printf("Available Devices:\n");
	
	const int maxPlatforms = 64;
	const int maxDevices = 64;
	cl_platform_id platformIDs[maxPlatforms];
	cl_device_id deviceIDs[maxDevices];
	
	cl_uint numPlatforms;
	E(clGetPlatformIDs(maxPlatforms, &platformIDs[0], &numPlatforms));
	
	const size_t bufSize = 1 << 14;
	char buf[bufSize];

	for (cl_uint i = 0; i < numPlatforms; ++i)
	{
		cl_uint numDevices;
		E(clGetDeviceIDs(platformIDs[i], CL_DEVICE_TYPE_ALL, maxDevices, &deviceIDs[0], &numDevices));
		
		const cl_device_info deviceInfo[5] =     { CL_DEVICE_PROFILE,   CL_DEVICE_VERSION,   CL_DEVICE_NAME,   CL_DEVICE_VENDOR,   CL_DEVICE_EXTENSIONS};
		for(cl_uint j = 0; j < numDevices; j++)
		{
			printf("Device %d:%d\n", i, j);
			for(int k = 0; k < 5; k++)
			{
				buf[0] = 0;
				E(clGetDeviceInfo(deviceIDs[j], deviceInfo[k], bufSize, &buf[0], NULL));
				printf("\t%s\n", buf);
			}
			printf("\n");
		}
	}
}

static int parsePlatformAndDevice(const char *str, unsigned int *platformIndex, unsigned int *deviceIndex)
{
	if(sscanf(str, "%u:%u", platformIndex, deviceIndex) != 2)
		return 0;
		
	return 1;
}

int main(int argc, char *argv[])
{
	const unsigned int maxContexts = 64;
	const unsigned int maxPlatforms = 64;
	const unsigned int maxDevices = 64;

	unsigned int numContexts = 0;
	context_and_queue_t contexts[maxContexts];
	
	cl_int err;
	cl_platform_id platformIDs[maxPlatforms];
	cl_uint numPlatforms;
	E(clGetPlatformIDs(maxPlatforms, &platformIDs[0], &numPlatforms));
	
	const char *outputFileName = NULL;
	bool useGUI = true;
	unsigned int width = 512;
	unsigned int height = 512;
	
	const char *realStr = NULL;
	const char *imagStr = NULL;
	const char *radiusStr = NULL;
	unsigned int maxIterations = 0;
	
	static struct option longOptions[] = 
	{
		{ "help",           no_argument,       0, '?' },
		{ "device",         required_argument, 0, 'd' },
		
		{ "width",          required_argument, 0, 'w' },
		{ "height",         required_argument, 0, 'h' },
		
		{ "no-gui",         no_argument,       0, 'n' },

		{ "real",           required_argument, 0, 'r' },
		{ "imag",           required_argument, 0, 'i' },
		{ "radius",         required_argument, 0, 's' },
		
		{ "max-iterations", required_argument, 0, 'l' },
		
		{ "out",            required_argument, 0, 'o' },
	};
	
	int optionIndex = 0;
	for(;;)
	{
		int c = getopt_long(argc, argv, "?nd:o:w:h:r:i:s:l:", longOptions, &optionIndex);
		
		if(c == -1)
			break;
		
		switch(c)
		{
			case '?':
			{
				print_usage();
				break;
			}
			
			case 'd':
			{
				if(numContexts == maxContexts)
				{
					printf("Maximum number of contexts reached. Omitted device \"%s\".\n", optarg);
					break;
				}
				
				if(!parsePlatformAndDevice(optarg, &contexts[numContexts].platformIndex, &contexts[numContexts].deviceIndex))
				{
					printf("Invalid platform/device specifier: \"%s\".\n", optarg);
					return 1;
				}
				
				if (contexts[numContexts].platformIndex >= numPlatforms)
				{
					printf("Invalid platform: %u (range is 0 to %d)\n", contexts[numContexts].platformIndex, numPlatforms - 1);
					return 1;
				}

				const cl_platform_id platformID = platformIDs[contexts[numContexts].platformIndex];

				cl_device_id deviceIDs[maxDevices];
				cl_uint numDevices;
				E(clGetDeviceIDs(platformID, CL_DEVICE_TYPE_ALL, maxDevices, &deviceIDs[0], &numDevices));

				if(contexts[numContexts].deviceIndex >= numDevices)
				{
					printf("Invalid device: %u (range is 0 to %d).\n", contexts[numContexts].deviceIndex, numDevices);
					return 1;
				}

				printf("Using device %u (platform %u).\n", contexts[numContexts].deviceIndex, contexts[numContexts].platformIndex);
				
				const cl_device_id deviceID = deviceIDs[contexts[numContexts].deviceIndex];

				// create context
				const cl_context_properties properties[] = { CL_CONTEXT_PLATFORM, (cl_context_properties) platformID, 0 };
				contexts[numContexts].context = clCreateContext(properties, 1, &deviceID, NULL, NULL, &err); E(err);

				// create command queue.
				contexts[numContexts].commandQueue = clCreateCommandQueue(contexts[numContexts].context, deviceID, 0, &err); E(err);
				
				++numContexts;
				break;
			}
			
			case 'n':
			{
				useGUI = false;
				break;
			}
			
			case 'o':
			{
				outputFileName = optarg;
				break;
			}
			
			case 'w':
			{
				width = (unsigned int)strtol(optarg, NULL, 10);
				if(width == 0)
				{
					printf("Invalid width.\n");
					return 1;
				}
				break;
			}
			
			case 'h':
			{
				height = (unsigned int)strtol(optarg, NULL, 10);
				if(height == 0)
				{
					printf("Invalid height.\n");
					return 1;
				}
				break;
			}
			
			case 'r':
			{
				realStr = optarg;
				break;
			}
			
			case 'i':
			{
				imagStr = optarg;
				break;
			}
			
			case 's':
			{
				radiusStr = optarg;
				break;
			}
			
			case 'l':
			{
				maxIterations = (unsigned int)strtol(optarg, NULL, 10);
				if(maxIterations == 0)
				{
					printf("Invalid number of iterations.\n");
					return 1;
				}
				break;
			}
			
			default:
			{
				printf("Invalid argument \"%c\".\n", c);
				return 1;
			}
		}
	}
		
	if(numContexts == 0)
	{
		printf("You must specify at least one device.\n\n");
		print_opencl_info();
		return 1;
	}
	
	
	mandelbrot_t m;
	mandelbrot_init(&m, 3, width, height);
	if(maxIterations > 0)
		mandelbrot_set_max_iterations(&m, maxIterations);
	mandelbrot_from_string(&m, realStr, imagStr, radiusStr);
	
	for(unsigned int i = 0; i < numContexts; ++i)
	{
		mandelbrot_add_opencl_processor(&m, contexts[i].commandQueue, 1 << 16, 1 << 12);
	}
	
	mandelbrot_glitch_init(&m, contexts[0].commandQueue);
	mandelbrot_start(&m);
	
#ifndef NOGUI
	if(useGUI)
	{
		gui_main(&m);
	}
#endif
	
	if(outputFileName != NULL)
	{
		mandelbrot_wait(&m);
		
		FILE *outputFile = fopen(outputFileName, "wb");
		
		width = mandelbrot_get_width(&m);
		height = mandelbrot_get_height(&m);
		
		fprintf(outputFile, "P6 %u %u 255 ", width, height);
		
		const uint32_t *image = mandelbrot_get_colors(&m);
		
		const unsigned int count = width * height;
		unsigned char *ppmData = (unsigned char *)malloc(count * 3);
		
		for(unsigned int i = 0; i < count; ++i)
		{
			ppmData[i * 3 + 0] = (unsigned char)(image[i] >> 16);
			ppmData[i * 3 + 1] = (unsigned char)(image[i] >> 8);
			ppmData[i * 3 + 2] = (unsigned char)(image[i] >> 0);
		}
		
		fwrite(ppmData, count * 3, 1, outputFile);
		
		fclose(outputFile);
		
		free(ppmData);
	}
	
	mandelbrot_stop(&m, true);
	mandelbrot_destroy(&m);
	
	for(unsigned int i = 0; i < numContexts; ++i)
	{
		E(clReleaseCommandQueue(contexts[i].commandQueue));
		E(clReleaseContext(contexts[i].context));
	}
}	
