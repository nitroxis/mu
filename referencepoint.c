#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <pthread.h>
#include <semaphore.h>
#include <mpfr.h>
#include <mpc.h>
#include <assert.h>

#include "referencepoint.h"

void referencepoint_init(referencepoint_t *ref, const unsigned int maxIterations, const unsigned int numCoefficients)
{
	ref->maxIterations = maxIterations;
	ref->numCoefficients = numCoefficients;
	ref->approximatedIterations = 0;
	ref->useApproximation = true;
	
	mpc_init2(ref->c, 53);
	mpc_init2(ref->z, 53);
	mpfr_init2(ref->radius, 53);
	mpfr_init2(ref->radius4, 53);
	
	ref->coefficientsHeap = (mpc_t *)malloc(sizeof(mpc_t) * numCoefficients * 4);
	ref->coefficients             = &ref->coefficientsHeap[numCoefficients * 0];
	ref->coefficients2            = &ref->coefficientsHeap[numCoefficients * 1];
	ref->coefficientsDerivatives  = &ref->coefficientsHeap[numCoefficients * 2];
	ref->coefficientsDerivatives2 = &ref->coefficientsHeap[numCoefficients * 3];
	
	for(unsigned int i = 0; i < numCoefficients * 4; i++)
		mpc_init2(ref->coefficientsHeap[i], 53);
	
	ref->thread = 0;
	
	sem_init(&ref->started, 0, 0);
	pthread_mutex_init(&ref->approximationMutex, NULL);
	
	atomic_store(&ref->iterations, 0);
	atomic_store(&ref->stop, false);

	ref->orbitDouble        = (double *)malloc(sizeof(double) * 2 * maxIterations);
	ref->coefficientsDouble = (double *)malloc(sizeof(double) * 2 * numCoefficients);
	//ref->coefficientsDerivatives = (double *)malloc(sizeof(double) * 2 * numCoefficients);
}

void referencepoint_destroy(referencepoint_t *ref)
{
	assert(ref->thread == 0);
	
	sem_destroy(&ref->started);
	pthread_mutex_destroy(&ref->approximationMutex);
	
	mpc_clear(ref->c);
	mpc_clear(ref->z);
	mpfr_clear(ref->radius);
	mpfr_clear(ref->radius4);
	
	free(ref->coefficientsHeap);
	
	free(ref->orbitDouble);
	free(ref->coefficientsDouble);
	//free(ref->coefficientsDerivatives);
}

void referencepoint_set(referencepoint_t *ref, const mpc_t c, const mpfr_t radius, const mpfr_prec_t precision)
{
	mpc_set_prec(ref->c, precision);
	mpc_set_prec(ref->z, precision);
	mpfr_set_prec(ref->radius, precision);
	mpfr_set_prec(ref->radius4, precision);
	
	mpc_set(ref->c, c, MPFR_RNDN);
	mpc_set(ref->z, c, MPFR_RNDN);
	
	mpfr_set(ref->radius, radius, MPFR_RNDN);
	mpfr_sqr(ref->radius4, ref->radius, MPFR_RNDN);
	mpfr_sqr(ref->radius4, ref->radius4, MPFR_RNDN);
	
	for(unsigned int i = 0; i < ref->numCoefficients; i++)
	{
		mpc_set_prec(ref->coefficients[i], precision);
		mpc_set_prec(ref->coefficients2[i], precision);
		mpc_set_prec(ref->coefficientsDerivatives[i], precision);
		mpc_set_prec(ref->coefficientsDerivatives2[i], precision);
		
		mpc_set_ui(ref->coefficients[i], i == 0 ? 1 : 0, MPFR_RNDN);
		mpc_set_ui(ref->coefficientsDerivatives[i], 0, MPFR_RNDN);
	}
	
	atomic_store(&ref->iterations, 0);
	ref->approximatedIterations = 0;
	ref->approximationAccurate = true;
	ref->escaped = false;
}

static void referencepoint_compute_coefficients(const unsigned int numCoefficients, mpc_t *input, mpc_t *output, mpc_t z, mpc_t temp)
{
	unsigned int i = numCoefficients;
	while(i-- > 0)
	{
		// output[i] = z * input[i]
		mpc_mul(output[i], z, input[i], MPFR_RNDN);

		// sum up first half.
		for(unsigned int j = 0; j < i / 2; ++j)
		{
			// output[i] += input[j] * input[i-j-1]
			mpc_fma(output[i], input[j], input[i - j - 1], output[i], MPFR_RNDN);
		}

		// output[i] *= 2
		// to account for the other half.
		mpc_mul_2ui(output[i], output[i], 1, MPFR_RNDN);
		
		// add the middle element.
		if(i & 1)
		{
			// coeff[i] += coeff[i / 2]^2
			mpc_sqr(temp, input[i / 2], MPFR_RNDN);
			mpc_add(output[i], output[i], temp, MPFR_RNDN);
		}
	}

	// coeff[0] += 1
	mpc_add_ui(output[0], output[0], 1, MPFR_RNDN);
}

static void referencepoint_compute_coefficients_derivatives(const unsigned int numCoefficients, mpc_t *input, mpc_t *output, mpc_t *coefficients, mpc_t z)
{
	unsigned int i = numCoefficients;
	while(i-- > 0)
	{
		// output[i] = z * input[i]
		mpc_mul(output[i], z, input[i], MPFR_RNDN);
					
		// output[i] += coefficients[0] * coefficients[i]
		mpc_fma(output[i], coefficients[0], coefficients[i], output[i], MPFR_RNDN);
		
		for(unsigned int j = 0; j < i; ++j)
		{
			// output[i] += coefficients[j] * input[i - j - 1]
			mpc_fma(output[i], coefficients[j], input[i - j - 1], output[i], MPFR_RNDN);
		}
		
		// output[i] *= 2
		mpc_mul_2ui(output[i], output[i], 1, MPFR_RNDN);
	}
}

void referencepoint_compute(referencepoint_t *ref)
{
	pthread_mutex_lock(&ref->approximationMutex);
	sem_post(&ref->started);
	
	const mpfr_prec_t prec = mpc_get_prec(ref->c);
	
	const unsigned int maxIterations = ref->maxIterations;
	const unsigned int numCoefficients = ref->numCoefficients;
	const unsigned int maxApproximation = ref->useApproximation ? maxIterations - 1 : 0;
	
	// temps.
	mpc_t temp;
	mpfr_t s;
	mpc_init2(temp, prec);
	mpfr_init2(s, prec);
	
	for(unsigned int n = atomic_load(&ref->iterations); n < maxIterations; ++n)
	{
		if(atomic_load(&ref->stop))
		{
			pthread_mutex_unlock(&ref->approximationMutex);
			break;
		}
		
		// store orbit in double precision.
		ref->orbitDouble[2 * n + 0] = mpfr_get_d(mpc_realref(ref->z), MPFR_RNDN);
		ref->orbitDouble[2 * n + 1] = mpfr_get_d(mpc_imagref(ref->z), MPFR_RNDN);
		atomic_fetch_add(&ref->iterations, 1);
		
		if(ref->approximationAccurate)
		{
			referencepoint_compute_coefficients_derivatives(numCoefficients, ref->coefficientsDerivatives,  ref->coefficientsDerivatives2, ref->coefficients, ref->z);
			referencepoint_compute_coefficients(numCoefficients, ref->coefficients, ref->coefficients2, ref->z, temp);
			
			// check if the approximation is still good.
			const double epsilon = 1e-40;
			
			// accurate = (|C|^2 / |A|^2 * delta^4) < epsilon
			mpc_norm(mpc_realref(temp), ref->coefficients2[2], MPFR_RNDN);
			mpc_norm(mpc_imagref(temp), ref->coefficients2[0], MPFR_RNDN);
			mpfr_div(mpc_realref(temp), mpc_realref(temp), mpc_imagref(temp), MPFR_RNDN);
			mpfr_mul(mpc_realref(temp), mpc_realref(temp), ref->radius4, MPFR_RNDN);
			bool accurate = mpfr_get_d(mpc_realref(temp), MPFR_RNDN) < epsilon;
			
			if(accurate)
			{
				// accurate = (|dC|^2 / |dA|^2 * delta^4) < epsilon
				mpc_norm(mpc_realref(temp), ref->coefficientsDerivatives2[2], MPFR_RNDN);
				mpc_norm(mpc_imagref(temp), ref->coefficientsDerivatives2[0], MPFR_RNDN);
				mpfr_div(mpc_realref(temp), mpc_realref(temp), mpc_imagref(temp), MPFR_RNDN);
				mpfr_mul(mpc_realref(temp), mpc_realref(temp), ref->radius4, MPFR_RNDN);
				accurate = mpfr_get_d(mpc_realref(temp), MPFR_RNDN) < epsilon;
			}
			
			if(!accurate || n >= maxApproximation)
			{
				// stop approximation.
				ref->approximationAccurate = false;

				// store coefficients and clean up.
				mpfr_set_prec(s, mpfr_get_prec(ref->radius));
				mpfr_set(s, ref->radius, MPFR_RNDN);
				for(unsigned int i = 0; i < numCoefficients; ++i)
				{
					mpc_mul_fr(temp, ref->coefficients[i], s, MPFR_RNDN);
					mpfr_mul(s, s, ref->radius, MPFR_RNDN);
					
					ref->coefficientsDouble[2 * i + 0] = mpfr_get_d(mpc_realref(temp), MPFR_RNDN);
					ref->coefficientsDouble[2 * i + 1] = mpfr_get_d(mpc_imagref(temp), MPFR_RNDN);
					
					//ref->coefficientsDerivativesDouble[2 * i + 0] = mpfr_get_d(mpc_realref(temp), MPFR_RNDN);
					//ref->coefficientsDerivativesDouble[2 * i + 1] = mpfr_get_d(mpc_imagref(temp), MPFR_RNDN);
				}

				// signal we're done.
				ref->approximatedIterations = n;
				pthread_mutex_unlock(&ref->approximationMutex);
			}
			else
			{
				// swap buffers.
				mpc_t *backup;
				backup = ref->coefficients; ref->coefficients = ref->coefficients2; ref->coefficients2 = backup;
				backup = ref->coefficientsDerivatives; ref->coefficientsDerivatives = ref->coefficientsDerivatives2; ref->coefficientsDerivatives2 = backup;
			}
		}

		// a = |Z|^2
		mpfr_sqr(mpc_realref(temp), mpc_realref(ref->z), MPFR_RNDN); // Z.r^2
		mpfr_sqr(mpc_imagref(temp), mpc_imagref(ref->z), MPFR_RNDN); // Z.i^2
		
		// check if |Z| < 2 (or |Z|^2 < 4)
		if(!ref->escaped)
		{
			mpfr_add(s, mpc_realref(temp), mpc_imagref(temp), MPFR_RNDN); // Z.r^2 + Z.i^2
			if(mpfr_cmp_ui(s, 4) >= 0)
			{
				ref->escapeTime = n;
				ref->escaped = true;
			}
		}
		
		// optimized Mandelbrot. Z = Z^2 + C
		// Z.i = (Z.r + Z.i)^2 - Z.r^2 - Z.i^2 + C.i
		mpfr_add(s, mpc_realref(ref->z), mpc_imagref(ref->z), MPFR_RNDN); // Z.r + Z.i
		mpfr_sqr(mpc_imagref(ref->z), s, MPFR_RNDN); // (Z.r + Z.i)^2
		mpfr_sub(mpc_imagref(ref->z), mpc_imagref(ref->z), mpc_realref(temp), MPFR_RNDN); // (Z.r + Z.i)^2 - Z.r^2
		mpfr_sub(mpc_imagref(ref->z), mpc_imagref(ref->z), mpc_imagref(temp), MPFR_RNDN); // (Z.r + Z.i)^2 - Z.r^2 - Z.i^2
		mpfr_add(mpc_imagref(ref->z), mpc_imagref(ref->z), mpc_imagref(ref->c), MPFR_RNDN); // (Z.r + Z.i)^2 - Z.r^2 - Z.i^2 + C.i

		// Z.r = Z.r^2 - Z.i^2 + C.r
		mpfr_sub(mpc_realref(ref->z), mpc_realref(temp), mpc_imagref(temp), MPFR_RNDN); // Z.r^2 - Z.i^2
		mpfr_add(mpc_realref(ref->z), mpc_realref(ref->z), mpc_realref(ref->c), MPFR_RNDN); // Z.r^2 - Z.i^2 + C.r
	}
	
	if(!ref->escaped)
	{
		ref->escapeTime = ref->maxIterations;
	}
	
	mpc_clear(temp);
	mpfr_clear(s);
}

static void *stub(void *arg)
{
	referencepoint_t *ref = (referencepoint_t *)arg;
	referencepoint_compute(ref);
	return NULL;
}

void referencepoint_start(referencepoint_t *ref)
{
	atomic_store(&ref->stop, false);
	pthread_create(&ref->thread, NULL, stub, ref);
	sem_wait(&ref->started);
}

void referencepoint_wait(referencepoint_t *ref)
{
	if(ref->thread == 0)
		return;
	
	pthread_join(ref->thread, NULL);
	ref->thread = 0;
}

void referencepoint_stop(referencepoint_t *ref, bool wait)
{
	atomic_store(&ref->stop, true);
	
	if(wait)
		referencepoint_wait(ref);
}

const double *referencepoint_get_coefficients(referencepoint_t *ref, unsigned int *numApproximatedIterations)
{
	pthread_mutex_lock(&ref->approximationMutex);
	
	if(numApproximatedIterations != NULL)
		*numApproximatedIterations = ref->approximatedIterations;
	
	const double *coefficients = ref->coefficientsDouble;
	
	pthread_mutex_unlock(&ref->approximationMutex);
	
	return coefficients;
}

const double *referencepoint_get_orbit(referencepoint_t *ref)
{
	return ref->orbitDouble;
}

unsigned int referencepoint_get_current_iterations(const referencepoint_t *ref)
{
	return atomic_load(&ref->iterations);
}

unsigned int referencepoint_get_escape_time(const referencepoint_t *ref)
{
	return ref->escapeTime;
}

bool referencepoint_is_escaped(const referencepoint_t *ref)
{
	return ref->escaped;
}