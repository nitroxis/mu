#include <string.h>
#include <stdatomic.h>
#include <CL/cl.h>

#include "clhelper.h"
#include "glitchanalyzer.h"
#include "stopwatch.h"

extern unsigned char glitchanalyzer_cl[];
extern unsigned int glitchanalyzer_cl_len;

void glitchanalyzer_init(glitchanalyzer_t *g, const cl_command_queue commandQueue, const unsigned int width, const unsigned int height)
{
	g->width = width;
	g->height = height;
	
	g->commandQueue = commandQueue;
	E(clGetCommandQueueInfo(commandQueue, CL_QUEUE_CONTEXT, sizeof(cl_context), &g->context, NULL));
	E(clGetCommandQueueInfo(commandQueue, CL_QUEUE_DEVICE, sizeof(cl_device_id), &g->deviceID, NULL));
	
	// compile program.
	printf("Creating glitch analyzer program... "); fflush(stdout);
	const char *src[1] = { (char *)glitchanalyzer_cl };
	const size_t lengths[1] = { glitchanalyzer_cl_len };
	
	cl_int err;
	g->program = clCreateProgramWithSource(g->context, 1, src, lengths, &err); E(err);

	printf("done.\n"); fflush(stdout);
	
	printf("Building glitch analyzer program... "); fflush(stdout);
	
	char args[256];
	sprintf(args, "-cl-finite-math-only -cl-no-signed-zeros -DWIDTH=%u -DHEIGHT=%u", width, height);

	err = clBuildProgram(g->program, 1, &g->deviceID, args, NULL, NULL);
	
	{
		// print log.
		const size_t logSize = 1 << 16;
		char *log = (char *)malloc(logSize);
		
		log[0] = 0;
		
		E(clGetProgramBuildInfo(g->program, g->deviceID, CL_PROGRAM_BUILD_LOG, logSize, &log[0], NULL));
		printf("done.\n%s\n", log); fflush(stdout);
		
		free(log);
	}
	
	E(err);	
	
	// create kernels.
	printf("Creating kernels... "); fflush(stdout);
	g->erodeKernel = clCreateKernel(g->program, "erode", &err); E(err);
	
	E(clGetKernelWorkGroupInfo(g->erodeKernel, g->deviceID, CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t), &g->erodeKernelSize, NULL));
	printf("done.\n"); fflush(stdout);
	
	//g->glitchImageSize = sizeof(uint32_t) * ((width + 31) / 32) * height;
	//g->glitchImage = (uint32_t *)malloc(g->glitchImageSize);
	
	g->glitchImageSize = width * height;
	g->glitchImage = (unsigned char *)malloc(g->glitchImageSize);
	
	
	// create buffers.
	printf("Creating buffers... "); fflush(stdout);
	g->glitchImageBuffer[0] = clCreateBuffer(g->context, CL_MEM_READ_WRITE, g->glitchImageSize,  NULL, &err); E(err);
	g->glitchImageBuffer[1] = clCreateBuffer(g->context, CL_MEM_READ_WRITE, g->glitchImageSize,  NULL, &err); E(err);
	g->infoBuffer           = clCreateBuffer(g->context, CL_MEM_READ_WRITE, 3 * sizeof(cl_uint), NULL, &err); E(err);
	printf("done.\n"); fflush(stdout);
	
	// set args.
	E(clSetKernelArg(g->erodeKernel, 2, sizeof(cl_mem),  &g->infoBuffer));
}

void glitchanalyzer_destroy(glitchanalyzer_t *g)
{
	E(clReleaseMemObject(g->glitchImageBuffer[0]));
	E(clReleaseMemObject(g->glitchImageBuffer[1]));
	E(clReleaseMemObject(g->infoBuffer));
	E(clReleaseKernel(g->erodeKernel));
	E(clReleaseProgram(g->program));
	
	free(g->glitchImage);
}

void glitchanalyzer_clear(glitchanalyzer_t *g)
{
	memset(g->glitchImage, 0, g->glitchImageSize);
}

void glitchanalyzer_find_blob(glitchanalyzer_t *g, unsigned int *x, unsigned int *y, atomic_bool *stop)
{
	//dumpImage(g);
	E(clEnqueueWriteBuffer(g->commandQueue, g->glitchImageBuffer[0], CL_TRUE, 0, g->glitchImageSize, g->glitchImage, 0, NULL, NULL));

	const size_t globalSize[2] = { g->width, g->height };
	
	int currentImage = 0;
	cl_uint info[3];
	cl_uint zero = 0;
	
	const int step = 2;
	
	do
	{
		if(atomic_load(stop))
			break;

		cl_event prevEvent;
		for(int i = 0; i < step; i++)
		{
			// setup.
			E(clSetKernelArg(g->erodeKernel, 0, sizeof(cl_mem), &g->glitchImageBuffer[currentImage]));
			E(clSetKernelArg(g->erodeKernel, 1, sizeof(cl_mem), &g->glitchImageBuffer[1 - currentImage]));
			
			// clear info buffer.
			info[0] = 0;
			cl_event cleared;
			E(clEnqueueWriteBuffer(g->commandQueue, g->infoBuffer, CL_FALSE, 0, sizeof(cl_uint), &zero, (i == 0) ? 0 : 1, (i == 0) ? NULL : &prevEvent, &cleared));
			
			if(i > 0)
				E(clReleaseEvent(prevEvent));
			
			// erode.
			cl_event eroded;
			E(clEnqueueNDRangeKernel(g->commandQueue, g->erodeKernel, 2, NULL, &globalSize[0], NULL, 1, &cleared, &eroded));
			E(clReleaseEvent(cleared));
			
			prevEvent = eroded;
			currentImage = 1 - currentImage;
			
			//E(clEnqueueReadBuffer(g->commandQueue, g->glitchImageBuffer[currentImage], CL_TRUE, 0, g->glitchImageSize, g->glitchImage, 1, &eroded, NULL));
			//dumpImage(g);
		}
		
		// read results.
		E(clEnqueueReadBuffer(g->commandQueue, g->infoBuffer, CL_TRUE, 0, 3 * sizeof(cl_int), &info[0], 1, &prevEvent, NULL));
		E(clReleaseEvent(prevEvent));
				
		*x = info[1];
		*y = info[2];
	} while(info[0] > 0);
}

