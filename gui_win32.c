#include <stdio.h>
#include <windows.h>
#include <Windowsx.h>
#include <math.h>

#include "mandelbrot.h"

static mandelbrot_t *mandelbrot;
static HBITMAP bitmap = NULL;
static int mouseX = 0;
static int mouseY = 0;

static void paste()
{
	OpenClipboard(NULL);
	HANDLE clip0 = GetClipboardData(CF_TEXT);
	char *clipboard = (char *)GlobalLock(clip0);
	
	mandelbrot_parse_sft(mandelbrot, clipboard);
	
	GlobalUnlock(clipboard);
	CloseClipboard();
}

static void copy()
{
	char *buf;
	int len = mpfr_asprintf (&buf, "r=%RNe\ni=%RNe\ns=%RNe\niteration_limit=%u", mpc_realref(mandelbrot->center), mpc_imagref(mandelbrot->center), mandelbrot->radius, mandelbrot->maxIterations);
	printf("%s\n", buf);
		
	HGLOBAL hMem =  GlobalAlloc(GMEM_MOVEABLE, len + 1);
	memcpy(GlobalLock(hMem), buf, len + 1);
	GlobalUnlock(hMem);
	OpenClipboard(NULL);
	EmptyClipboard();
	SetClipboardData(CF_TEXT, hMem);
	CloseClipboard();
	
	mpfr_free_str(buf);
}

static WINDOWPLACEMENT oldWindowPlacement;

static void toggle_fullscreen(HWND hWnd)
{
	oldWindowPlacement.length = sizeof(WINDOWPLACEMENT);
	
	DWORD dwStyle = GetWindowLong(hWnd, GWL_STYLE);
	if (dwStyle & WS_OVERLAPPEDWINDOW)
	{
		MONITORINFO mi;
		mi.cbSize = sizeof(MONITORINFO);
		if (GetWindowPlacement(hWnd, &oldWindowPlacement) && GetMonitorInfo(MonitorFromWindow(hWnd, MONITOR_DEFAULTTOPRIMARY), &mi))
		{
			SetWindowLong(hWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(hWnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,	mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		}
	}
	else
	{
		SetWindowLong(hWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(hWnd, &oldWindowPlacement);
		SetWindowPos(hWnd, NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |	SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
	}

}

LRESULT CALLBACK window_proc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
    switch(Msg)
    {
		case WM_CREATE:
		{
			SetTimer(hWnd, 1337, 30, NULL);
			break;
		}
		
		case WM_DESTROY:
		{
			PostQuitMessage(WM_QUIT);
			break;
		}
		
		case WM_TIMER:
		{
			InvalidateRect(hWnd, NULL, false);
			break;
		}
		
		case WM_SIZE:
		{
			RECT rect;
			GetClientRect(hWnd, &rect);
			unsigned int newWidth = (unsigned int)(rect.right - rect.left);
			unsigned int newHeight = (unsigned int)(rect.bottom - rect.top);
			
			if(newWidth != 0 && newHeight != 0 && (newWidth != mandelbrot_get_width(mandelbrot) || newHeight != mandelbrot_get_height(mandelbrot)))
			{
				mandelbrot_stop(mandelbrot, true);
				mandelbrot_set_size(mandelbrot, newWidth, newHeight);
				
				if(bitmap != NULL)
				{
					DeleteObject(bitmap);
					bitmap = NULL;
				}

				printf("Size: %u x %u\n", newWidth, newHeight); fflush(stdout);
				mandelbrot_start(mandelbrot);
			}
			
			break;
		}
		
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hDC = BeginPaint(hWnd, &ps);
			
			const unsigned int width = mandelbrot_get_width(mandelbrot);
			const unsigned int height = mandelbrot_get_height(mandelbrot);
			
			if(bitmap == NULL)
			{
				bitmap = CreateBitmap(width, height, 1, 32, NULL);
			}
				
			if(!SetBitmapBits(bitmap, 4 * width * height, mandelbrot_get_colors(mandelbrot)))
			{
				printf("Failed to set bitmap bits.\n");
			}
			
			HDC hDCMem = CreateCompatibleDC(hDC);
			SelectObject(hDCMem, bitmap);

			BITMAP bm;
			GetObject(bitmap, sizeof(bm), &bm);

			BitBlt(hDC, 0, 0, bm.bmWidth, bm.bmHeight, hDCMem, 0, 0, SRCCOPY);
			DeleteDC(hDCMem);
			
			EndPaint(hWnd, &ps);
			break;
		}
		
		case WM_KEYUP:
		{
			switch(wParam)
			{
				case VK_ESCAPE:
					DestroyWindow(hWnd);
					break;
				
				case VK_SPACE:
					mandelbrot_stop(mandelbrot, true);
					mandelbrot_reset(mandelbrot);
					mandelbrot_start(mandelbrot);
					break;
				
				case VK_ADD:
					mandelbrot_stop(mandelbrot, true);
					mandelbrot_set_max_iterations(mandelbrot, mandelbrot_get_max_iterations(mandelbrot) * 2);
					mandelbrot_start(mandelbrot);
					break;
				
				case VK_SUBTRACT:
					mandelbrot_stop(mandelbrot, true);
					unsigned int max = mandelbrot_get_max_iterations(mandelbrot) / 2;
					if(max < 1)
						max = 1;
					
					mandelbrot_set_max_iterations(mandelbrot, max);
					mandelbrot_start(mandelbrot);
					break;
					
				case VK_F11:
				case VK_RETURN:
					toggle_fullscreen(hWnd);
					break;
					
				case 'R':
					mandelbrot_stop(mandelbrot, true);
					mandelbrot->findGoodReference = !mandelbrot->findGoodReference;
					printf("Reference atom search: %s\n", mandelbrot->findGoodReference ? "on" : "off");
					mandelbrot_start(mandelbrot);
					break;
					
				case 'A':
					mandelbrot_stop(mandelbrot, true);
					bool useApprox = !mandelbrot_get_use_approximation(mandelbrot);
					mandelbrot_set_use_approximation(mandelbrot, useApprox);
					printf("Series approximation: %s\n", useApprox ? "on" : "off");
					mandelbrot_start(mandelbrot);
					break;
					
				case 'V':
					mandelbrot_stop(mandelbrot, true);
					paste();
					mandelbrot_start(mandelbrot);
					break;
				
				case 'C':
					copy();
					break;
			}
			
			break;
		}
			
		case WM_LBUTTONDBLCLK:
		{
			mouseX = GET_X_LPARAM(lParam);
			mouseY = GET_Y_LPARAM(lParam);
	
			const unsigned int width = mandelbrot_get_width(mandelbrot);
			const unsigned int height = mandelbrot_get_height(mandelbrot);
			const double factor = 2.0 / (width > height ? height : width);
	
			const double x = ((double)mouseX - width * 0.5) * factor;
			const double y = ((double)mouseY - height * 0.5) * factor;
			mandelbrot_stop(mandelbrot, true);
			mandelbrot_move(mandelbrot, x, y);
			mandelbrot_start(mandelbrot);
		
			break;
		}
		
		case WM_MOUSEMOVE:
		{
			int newX = GET_X_LPARAM(lParam);
			int newY = GET_Y_LPARAM(lParam);
						
			if(wParam & MK_LBUTTON && (newX != mouseX || newY != mouseY))
			{
				const unsigned int width = mandelbrot_get_width(mandelbrot);
				const unsigned int height = mandelbrot_get_height(mandelbrot);
				const double factor = 2.0 / (width > height ? height : width);
				
				const double x = (newX - mouseX) * factor;
				const double y = (newY - mouseY) * factor;
				mandelbrot_stop(mandelbrot, true);
				mandelbrot_move(mandelbrot, -x, -y);
				mandelbrot_start(mandelbrot);
			}
		
			mouseX = newX;
			mouseY = newY;
			
			break;
		}
		
		case WM_MOUSEWHEEL:
		{
			const unsigned int width = mandelbrot_get_width(mandelbrot);
			const unsigned int height = mandelbrot_get_height(mandelbrot);
			const double factor = 2.0 / (width > height ? height : width);

			const double x = ((double)mouseX - width * 0.5) * factor;
			const double y = ((double)mouseY - height * 0.5) * factor;
			const double zoom = pow(0.5, (double)GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA);
			mandelbrot_stop(mandelbrot, true);
			mandelbrot_zoom(mandelbrot, x, y, zoom);
			mandelbrot_auto_precision(mandelbrot);
			mandelbrot_start(mandelbrot);
		
			break;
		}
		
		default:
			return DefWindowProc(hWnd, Msg, wParam, lParam);
    }
    return 0;
}

void gui_main(mandelbrot_t *m)
{
	mandelbrot = m;

	WNDCLASSEX wndClass;
	wndClass.cbSize        = sizeof(wndClass);
	wndClass.style         = CS_OWNDC | CS_VREDRAW | CS_HREDRAW | CS_DBLCLKS;
	wndClass.lpfnWndProc   = window_proc;
	wndClass.cbClsExtra    = 0;
	wndClass.cbWndExtra    = 0;
	wndClass.hInstance     = NULL;
	wndClass.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndClass.lpszMenuName  = NULL;
	wndClass.lpszClassName = "Mandelbrot";
	wndClass.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);
	RegisterClassEx(&wndClass);

	RECT rect;
	rect.left = 0;
	rect.top = 0;
	rect.right = mandelbrot_get_width(m);
	rect.bottom = mandelbrot_get_height(m);
    
	const DWORD dwStyle = WS_OVERLAPPEDWINDOW | WS_VISIBLE;
	const DWORD dwExStyle = 0;
	AdjustWindowRectEx(&rect, dwStyle, FALSE, dwExStyle);
	CreateWindowEx(dwExStyle, wndClass.lpszClassName, "Mandelbrot with perturbation and series approximation", dwStyle, CW_USEDEFAULT, CW_USEDEFAULT, rect.right - rect.left, rect.bottom - rect.top, NULL, NULL, NULL, NULL);
	
	MSG msg;
	while(GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	
	if(bitmap != NULL)
		DeleteObject(bitmap);
}

