kernel void erode(global const char *input, global char *output, global uint *info)
{
	const uint x = (uint)get_global_id(0);
	const uint y = (uint)get_global_id(1);
	if(x >= WIDTH || y >= HEIGHT)
		return;
	
	const uint i = y * WIDTH + x;
	
	if(input[i] == 0)
	{
		output[i] = 0;
		return;
	}
	
	// is this the first pixel?
	if(atomic_inc(&info[0]) == 0)
	{
		// store it, just in case it is also the last.
		info[1] = x;
		info[2] = y;
	}

	// pixels on the image border can't have 8 neighbors.
	if(x == 0 || y == 0 || x == WIDTH - 1 || y == HEIGHT - 1)
	{
		output[i] = 0;
	}
	else
	{
		output[i] = input[i - 1 - WIDTH] & input[i - WIDTH] & input[i + 1 - WIDTH]
		          & input[i - 1]                            & input[i + 1]
		          & input[i - 1 + WIDTH] & input[i + WIDTH] & input[i + 1 + WIDTH];
	}
}

/*kernel void erode32(global const uint *input, global uint *output, global uint *info)
{
	const uint i = (uint)get_global_id(0);
	if(i >= WIDTH * HEIGHT)
		return;

	const uint myCell = i >> 5;
	const uint myBit = i & 31;
	const uint myMask = 1u << myBit;
	
	if((input[myCell] & myMask) == 0)
	{
		atomic_and(&output[myCell], ~myMask);
		return;
	}
	
	const uint x = i % WIDTH;
	const uint y = i / WIDTH;
	
	// is this the first pixel?
	if(atomic_inc(&info[0]) == 0)
	{
		// store it, just in case it is also the last.
		info[1] = x;
		info[2] = y;
	}

	// border pixels get killed instantly.
	if(x == 0 || y == 0 || x == WIDTH - 1 || y == HEIGHT - 1)
	{
		atomic_and(&output[myCell], ~myMask);
	}
	else
	{
		uint left = myCell;
		uint right = myCell;
		uint leftBit = myBit - 1;
		uint rightBit = myBit + 1;

		if(myBit == 0)
		{
			left = myCell - 1;
			leftBit = 31;
		}
		else if (myBit == 31)
		{
			right = myCell + 1;
			rightBit = 0;
		}

		const int s = WIDTH >> 5;

		// sum neighborhood.
		const uint c = ((input[left-s] >> leftBit) & 1) + ((input[myCell-s] >> myBit) & 1) + ((input[right-s] >> rightBit) & 1)
		             + ((input[left]   >> leftBit) & 1)                                    + ((input[right]   >> rightBit) & 1)
		             + ((input[left+s] >> leftBit) & 1) + ((input[myCell+s] >> myBit) & 1) + ((input[right+s] >> rightBit) & 1);
		
		if(c == 8)
			atomic_or(&output[myCell], myMask);
		else
			atomic_and(&output[myCell], ~myMask);
	}
}*/
