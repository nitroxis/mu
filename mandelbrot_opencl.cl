#pragma OPENCL EXTENSION cl_khr_fp64: enable

#define ESCAPED_BIT ((uint)1 << 31)
#define ITERATIONS_MASK (ESCAPED_BIT - 1)
#define GLITCHED (0xFFFFFFFF)

typedef ushort2 position_t;

// complex number stuff.
typedef double2 complex;
#define real(c) (c.x)
#define imag(c) (c.y)
#define cmul(a, b) (complex)(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x)
#define csqr(c) (complex)(c.x * c.x - c.y * c.y, 2 * c.x * c.y)
#define cmag2(c) (dot(c, c))

// state of an instance (i.e. pixel).
typedef struct
{
	complex d0;
	complex d;
} state_t;

kernel void initialize(
	  global const position_t *positions,
	  global       state_t    *state,
	  global       uint       *iterations,
	constant const complex    *coefficients,
	         const uint       approximatedIterations, 
	         const complex    offset,
	         const double     pixelSize,
	         const double     scale,
	         const uint       count)
{
	const uint i = (uint)get_global_id(0);
	if(i >= count)
		return;
	
	const complex p = offset + convert_double2(positions[i]); // pixel position relative to reference point.
	const complex c0 = p * scale; // normalized position.
	complex d = state[i].d0 = p * pixelSize;; // starting point.

	if(approximatedIterations > 0)
	{
		complex c = c0;
		d = (complex)0.0;
		for (int j = 0; j < NUM_COEFFICIENTS; ++j)
		{
			d += cmul(coefficients[j], c);
			c = cmul(c, c0);
		}
	}	
	
	state[i].d = d;
	iterations[i] = approximatedIterations;
}

kernel void iterate(
	global       state_t    *state, 
	global       uint       *iterations,
	global const complex    *referenceOrbit,
	       const uint       maxIterations, 
	       const uint       count)
{
	const uint i = (uint)get_global_id(0);
	if(i >= count)
		return;

	// state.
	const complex d0 = state[i].d0;
	complex d = state[i].d;
	uint n = iterations[i];
	if(n & ESCAPED_BIT) // escaped, stop.
		return;
	
	for (uint m = 0; m < maxIterations; ++m, ++n)
	{
		const complex x = referenceOrbit[m];
		const complex y = x + d;
		const double ymag2 = cmag2(y);
		
		// check for glitch.
		if (cmag2(x) * 1e-6 > ymag2)
		{
			// mark as glitch.
			n = GLITCHED;
			break;
		}
		
		// bailout.
		if (ymag2 >= 4.0)
		{
			n |= ESCAPED_BIT;
			break;
		}
		
		d = 2.0 * cmul(x, d) + csqr(d) + d0;
	}
	
	// store state.
	state[i].d = d;
	iterations[i] = n;
}

