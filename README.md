mu
==
A fast OpenCL-based Mandelbrot renderer with [perturbation and series approximation](https://mathr.co.uk/mandelbrot/perturbation.pdf), written in C99.

Compiling
---------
Linux/Unix: `make`

Windows 32-bit: `make SYSTEM=win32`

Windows 64-bit: `make SYSTEM=win64`

You need OpenCL, gmp, mpfr, mpc.
On Linux, SDL2 is required for the GUI.
On Windows, [msys2](https://msys2.github.io/) with the mingw32 or mingw64 toolchains can be used to compile the program.

Usage
-----
Run `./mu` without options to get a list of OpenCL devices on your computer.
Then run `./mu -dx:y` where x and y are the platform/device IDs from the list that you want to use.
You can use multiple devices to speed up rendering.
However, the most powerful device should come first as it is used as default device and might do most of the work.
The OpenCL device must support the `cl_khr_fp64` extension for double-precision. Virtually all OpenCL-capable dedicated GPUs (AMD, NVIDIA) should support it. Intel processors are fine, but most older Intel HD graphics won't work. However, Skylake (Intel HD 530) and newer appear to have it.

You can also use the program without a GUI (i.e. as a pure command line tool) using `-n` or `--no-gui`.
The image size can be specified with `-w` and `-h` (or `--width` and `--height`).
The output file is saved as portable pixel map (ppm) and set with `-o` or `--out`. 

Use the `-r`, `-i` and `-s` options to specify the coordinates and zoom of the region you want to render.
You can also set the maximum number of iterations using `-l` or `--max-iterations`.

Controls
--------
Hold left mouse button and move the mouse to move around.
Double click to focus/center on the clicked pixel.
Use mouse wheel to zoom.

| Key       | Function                           |
| --------: | ---------------------------------- |
| Escape    | Exit                               |
| Space	    | Reset                              |
| +         | Increase maximum iteration count   |
| -         | Decrease maximum iteration count   |
| R         | Toggle reference point search      |
| A         | Toggle series approximation        |
| F11/Enter | Toggle full screen                 |

Example
-------
Rendering a 2048x2048 image with 15756 iterations on device 0 of platform 1 at the specified location (without GUI):
```
./mu -d 1:0 -n -w 2048 -h 2048 -l 15756 -o babybeans.ppm -s "9.0E-76" \
-r "-1.985548413352953418245678803517026040561987431985854276276406746845711126058993484081547613333480219101280958482E+00" \
-i "2.743067126729694556175287646032154237263455024187599021148859129476783239240544859414368041154125023E-13"
```
Results in this image:
![Babybeans](https://s.nxs.re/babybeans.jpg)


License
-------
GPLv3, see LICENSE.

Credits
-------
Thanks to [Claude Heiland-Allen](http://mathr.co.uk/blog/) for his [mandelbrot-delta-cl](https://code.mathr.co.uk/fractal-bits/tree/HEAD:/mandelbrot-delta-cl) program.
This project started as a modification of his program and still uses code from it.