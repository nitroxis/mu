// mandelbrot-delta-cl -- Mandelbrot set perturbation renderer using OpenCL
// Copyright: (C) 2013 Claude Heiland-Allen <claude@mathr.co.uk>
// License: http://www.gnu.org/licenses/gpl.html‎ GPLv3+

#include <math.h>
#include <mpfr.h>
#include <mpc.h>

#include "atom.h"

unsigned int find_period(const mpc_t c0, const mpfr_t r, const unsigned int maxIterations)
{
	const mpfr_prec_t prec = mpfr_get_prec(r);
	mpc_t c[4], z[4], d, temp;
	
	mpc_init2(d, prec); 
	mpc_init2(temp, prec);
	
	for(int i = 0; i < 4; i++)
	{
		mpc_init2(c[i], prec);
		mpc_init2(z[i], prec);
		
		mpc_set(c[i], c0, MPFR_RNDN);
		mpc_set_ui(z[i], 0, MPFR_RNDN);
	}

	// rectangular region.
	mpfr_sub(mpc_realref(c[0]), mpc_realref(c[0]), r, MPFR_RNDN);
	mpfr_sub(mpc_imagref(c[0]), mpc_imagref(c[0]), r, MPFR_RNDN);
	
	mpfr_add(mpc_realref(c[1]), mpc_realref(c[1]), r, MPFR_RNDN);
	mpfr_sub(mpc_imagref(c[1]), mpc_imagref(c[1]), r, MPFR_RNDN);
	
	mpfr_add(mpc_realref(c[2]), mpc_realref(c[2]), r, MPFR_RNDN);
	mpfr_add(mpc_imagref(c[2]), mpc_imagref(c[2]), r, MPFR_RNDN);
	
	mpfr_sub(mpc_realref(c[3]), mpc_realref(c[3]), r, MPFR_RNDN);
	mpfr_add(mpc_imagref(c[3]), mpc_imagref(c[3]), r, MPFR_RNDN);
	
	unsigned int period = 0;
	for (unsigned int p = 1; p < maxIterations; ++p)
	{
		for (int i = 0; i < 4; ++i)
		{
			mpc_sqr(z[i], z[i], MPFR_RNDN);
			mpc_add(z[i], z[i], c[i], MPFR_RNDN);
		}

		int preal = 0;
		for (int i = 0; i < 4; ++i)
		{
			int j = (i + 1) % 4;
			
			if (!(mpfr_sgn(mpc_imagref(z[i])) < 0 && mpfr_sgn(mpc_imagref(z[j])) < 0) &&
			    !(mpfr_sgn(mpc_imagref(z[i])) > 0 && mpfr_sgn(mpc_imagref(z[j])) > 0))
			{
				mpc_sub(d, z[j], z[i], MPFR_RNDN);
				mpfr_mul(mpc_realref(temp), mpc_imagref(d), mpc_realref(z[i]), MPFR_RNDN);
				mpfr_mul(mpc_imagref(temp), mpc_realref(d), mpc_imagref(z[i]), MPFR_RNDN);
				mpfr_sub(mpc_realref(temp), mpc_realref(temp), mpc_imagref(temp), MPFR_RNDN);
				mpfr_mul(mpc_realref(temp), mpc_realref(temp), mpc_imagref(d), MPFR_RNDN);
				
				if (mpfr_sgn(mpc_realref(temp)) > 0)
					++preal;
			}
		}
		if (preal & 1)
		{
			period = p;
			break;
		}
	}
	
	// cleanup.
	for(int i = 0; i < 4; i++)
	{
		mpc_clear(c[i]);
		mpc_clear(z[i]);
	}
	mpc_clear(d);
	mpc_clear(temp);
	
	return period;
}

static int muatom(int period, mpfr_t x, mpfr_t y, mpfr_t z)
{
	const mpfr_prec_t accuracy = 32;
	mpfr_prec_t bits = mpfr_get_prec(x);
	if(mpfr_get_prec(y) > bits)	bits = mpfr_get_prec(y);
	
	#define VARS nx, ny, bx, by, wx, wy, zz, Ax, Ay, Bx, By, Cx, Cy, Dx, Dy, Ex, Ey, t1, t2, t3, t4, t5, t6, t7, t8, t9, t0, t01, t02, t03, t04
	mpfr_t VARS;
	mpfr_inits2(bits, VARS, (mpfr_ptr) 0);
	mpfr_set(nx, x, MPFR_RNDN);
	mpfr_set(ny, y, MPFR_RNDN);
	mpfr_set_prec(zz, mpfr_get_prec(z));
	int n_ok, w_ok, b_ok;
	int r = 0;
	do
	{
		mpfr_set_prec(t0,  bits - accuracy);
		mpfr_set_prec(t01, bits - accuracy);
		mpfr_set_prec(t02, bits - accuracy);
		mpfr_set_prec(t03, bits - accuracy);
		mpfr_set_prec(t04, bits - accuracy);
		int limit = 50;
		do
		{
			// refine nucleus
			mpfr_set_ui(Ax, 0, MPFR_RNDN);
			mpfr_set_ui(Ay, 0, MPFR_RNDN);
			mpfr_set_ui(Dx, 0, MPFR_RNDN);
			mpfr_set_ui(Dy, 0, MPFR_RNDN);
			for (int i = 0; i < period; ++i) {
			// D <- 2AD + 1  : Dx <- 2 (Ax Dx - Ay Dy) + 1 ; Dy = 2 (Ax Dy + Ay Dx)
			mpfr_mul(t1, Ax, Dx, MPFR_RNDN);
			mpfr_mul(t2, Ay, Dy, MPFR_RNDN);
			mpfr_mul(t3, Ax, Dy, MPFR_RNDN);
			mpfr_mul(t4, Ay, Dx, MPFR_RNDN);
			mpfr_sub(Dx, t1, t2, MPFR_RNDN);
			mpfr_mul_2ui(Dx, Dx, 1, MPFR_RNDN);
			mpfr_add_ui(Dx, Dx, 1, MPFR_RNDN);
			mpfr_add(Dy, t3, t4, MPFR_RNDN);
			mpfr_mul_2ui(Dy, Dy, 1, MPFR_RNDN);
			// A <- A^2 + n  : Ax <- Ax^2 - Ay^2 + nx ; Ay <- 2 Ax Ay + ny
			mpfr_sqr(t1, Ax, MPFR_RNDN);
			mpfr_sqr(t2, Ay, MPFR_RNDN);
			mpfr_mul(t3, Ax, Ay, MPFR_RNDN);
			mpfr_sub(Ax, t1, t2, MPFR_RNDN);
			mpfr_add(Ax, Ax, nx, MPFR_RNDN);
			mpfr_mul_2ui(t3, t3, 1, MPFR_RNDN);
			mpfr_add(Ay, t3, ny,MPFR_RNDN);
			}
			// n <- n - A / D  = (nx + ny i) - ((Ax Dx + Ay Dy) + (Ay Dx - Ax Dy)i) / (Dx^2 + Dy^2)
			mpfr_sqr(t1, Dx, MPFR_RNDN);
			mpfr_sqr(t2, Dy, MPFR_RNDN);
			mpfr_add(t1, t1, t2, MPFR_RNDN);

			mpfr_mul(t2, Ax, Dx, MPFR_RNDN);
			mpfr_mul(t3, Ay, Dy, MPFR_RNDN);
			mpfr_add(t2, t2, t3, MPFR_RNDN);
			mpfr_div(t2, t2, t1, MPFR_RNDN);
			mpfr_sub(t2, nx, t2, MPFR_RNDN);

			mpfr_mul(t3, Ay, Dx, MPFR_RNDN);
			mpfr_mul(t4, Ax, Dy, MPFR_RNDN);
			mpfr_sub(t3, t3, t4, MPFR_RNDN);
			mpfr_div(t3, t3, t1, MPFR_RNDN);
			mpfr_sub(t3, ny, t3, MPFR_RNDN);

			mpfr_set(t01, t2, MPFR_RNDN);
			mpfr_set(t02, t3, MPFR_RNDN);
			mpfr_set(t03, nx, MPFR_RNDN);
			mpfr_set(t04, ny, MPFR_RNDN);
			mpfr_sub(t01, t01, t03, MPFR_RNDN);
			mpfr_sub(t02, t02, t04, MPFR_RNDN);
			mpfr_add_ui(t01, t01, 1, MPFR_RNDN);
			mpfr_sub_ui(t01, t01, 1, MPFR_RNDN);
			mpfr_add_ui(t02, t02, 1, MPFR_RNDN);
			mpfr_sub_ui(t02, t02, 1, MPFR_RNDN);
			n_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);

			mpfr_set(nx, t2, MPFR_RNDN);
			mpfr_set(ny, t3, MPFR_RNDN);
		} while (!n_ok && --limit);
		
		if (!limit) goto cleanup;
		
		mpfr_set(wx, nx, MPFR_RNDN);
		mpfr_set(wy, ny, MPFR_RNDN);
		mpfr_set(bx, nx, MPFR_RNDN);
		mpfr_set(by, ny, MPFR_RNDN);
		limit = 50;
		do
		{
			// refine bond
			mpfr_set(Ax, wx, MPFR_RNDN);
			mpfr_set(Ay, wy, MPFR_RNDN);
			mpfr_set_ui(Bx, 1, MPFR_RNDN);
			mpfr_set_ui(By, 0, MPFR_RNDN);
			mpfr_set_ui(Cx, 0, MPFR_RNDN);
			mpfr_set_ui(Cy, 0, MPFR_RNDN);
			mpfr_set_ui(Dx, 0, MPFR_RNDN);
			mpfr_set_ui(Dy, 0, MPFR_RNDN);
			mpfr_set_ui(Ex, 0, MPFR_RNDN);
			mpfr_set_ui(Ey, 0, MPFR_RNDN);
			for (int i = 0; i < period; ++i)
			{
				// E <- 2 ( A E + B D )
				mpfr_mul(t1, Ax, Ex, MPFR_RNDN);
				mpfr_mul(t2, Ay, Ey, MPFR_RNDN);
				mpfr_sub(t1, t1, t2, MPFR_RNDN);
				mpfr_mul(t2, Ax, Ey, MPFR_RNDN);
				mpfr_mul(t3, Ay, Ex, MPFR_RNDN);
				mpfr_add(t2, t2, t3, MPFR_RNDN);
				mpfr_mul(t3, Bx, Dx, MPFR_RNDN);
				mpfr_mul(t4, By, Dy, MPFR_RNDN);
				mpfr_sub(t3, t3, t4, MPFR_RNDN);
				mpfr_add(Ex, t1, t3, MPFR_RNDN);
				mpfr_mul(t3, Bx, Dy, MPFR_RNDN);
				mpfr_mul(t4, By, Dx, MPFR_RNDN);
				mpfr_add(t3, t3, t4, MPFR_RNDN);
				mpfr_add(Ey, t2, t3, MPFR_RNDN);
				mpfr_mul_2ui(Ex, Ex, 1, MPFR_RNDN);
				mpfr_mul_2ui(Ey, Ey, 1, MPFR_RNDN);
				// D <- 2 A D + 1
				mpfr_mul(t1, Ax, Dx, MPFR_RNDN);
				mpfr_mul(t2, Ay, Dy, MPFR_RNDN);
				mpfr_mul(t3, Ax, Dy, MPFR_RNDN);
				mpfr_mul(t4, Ay, Dx, MPFR_RNDN);
				mpfr_sub(Dx, t1, t2, MPFR_RNDN);
				mpfr_mul_2ui(Dx, Dx, 1, MPFR_RNDN);
				mpfr_add_ui(Dx, Dx, 1, MPFR_RNDN);
				mpfr_add(Dy, t3, t4, MPFR_RNDN);
				mpfr_mul_2ui(Dy, Dy, 1, MPFR_RNDN);
				// C <- 2 ( B^2 + A C )
				mpfr_sqr(t1, Bx, MPFR_RNDN);
				mpfr_sqr(t2, By, MPFR_RNDN);
				mpfr_sub(t1, t1, t2, MPFR_RNDN);
				mpfr_mul(t2, Bx, By, MPFR_RNDN);
				mpfr_mul_2ui(t2, t2, 1, MPFR_RNDN);
				mpfr_mul(t3, Ax, Cx, MPFR_RNDN);
				mpfr_mul(t4, Ay, Cy, MPFR_RNDN);
				mpfr_sub(t3, t3, t4, MPFR_RNDN);
				mpfr_add(t1, t1, t3, MPFR_RNDN);
				mpfr_mul(t3, Ax, Cy, MPFR_RNDN);
				mpfr_mul(t4, Ay, Cx, MPFR_RNDN);
				mpfr_add(t3, t3, t4, MPFR_RNDN);
				mpfr_add(t2, t2, t3, MPFR_RNDN);
				mpfr_mul_2ui(Cx, t1, 1, MPFR_RNDN);
				mpfr_mul_2ui(Cy, t2, 1, MPFR_RNDN);
				// B <- 2 A B
				mpfr_mul(t1, Ax, Bx, MPFR_RNDN);
				mpfr_mul(t2, Ay, By, MPFR_RNDN);
				mpfr_mul(t3, Ax, By, MPFR_RNDN);
				mpfr_mul(t4, Ay, Bx, MPFR_RNDN);
				mpfr_sub(Bx, t1, t2, MPFR_RNDN);
				mpfr_mul_2ui(Bx, Bx, 1, MPFR_RNDN);
				mpfr_add(By, t3, t4, MPFR_RNDN);
				mpfr_mul_2ui(By, By, 1, MPFR_RNDN);
				// A <- A^2 + b
				mpfr_sqr(t1, Ax, MPFR_RNDN);
				mpfr_sqr(t2, Ay, MPFR_RNDN);
				mpfr_mul(t3, Ax, Ay, MPFR_RNDN);
				mpfr_sub(Ax, t1, t2, MPFR_RNDN);
				mpfr_add(Ax, Ax, bx, MPFR_RNDN);
				mpfr_mul_2ui(t3, t3, 1, MPFR_RNDN);
				mpfr_add(Ay, t3, by,MPFR_RNDN);
			}
			// (w) <- (w) - (B-1  D)^-1 (A - w)
			// (b) <- (b)   ( C   E)    (B + 1)
			//
			// det = (B-1)E - CD
			// inv = (E    -D)
			//       (-C (B-1) / det
			//
			// w - (E(A-w) - D(B+1))/det
			// b - (-C(A-w) + (B-1)(B+1))/det   ; B^2 - 1
			//
			// A -= w
			mpfr_sub(Ax, Ax, wx, MPFR_RNDN);
			mpfr_sub(Ay, Ay, wy, MPFR_RNDN);
			// (t1,t2) = B^2 - 1
			mpfr_mul(t1, Bx, Bx, MPFR_RNDN);
			mpfr_mul(t2, By, By, MPFR_RNDN);
			mpfr_sub(t1, t1, t2, MPFR_RNDN);
			mpfr_sub_ui(t1, t1, 1, MPFR_RNDN);
			mpfr_mul(t2, Bx, By, MPFR_RNDN);
			mpfr_mul_2ui(t2, t2, 1, MPFR_RNDN);
			// (t1,t2) -= C(A-w)
			mpfr_mul(t3, Cx, Ax, MPFR_RNDN);
			mpfr_mul(t4, Cy, Ay, MPFR_RNDN);
			mpfr_sub(t3, t3, t4, MPFR_RNDN);
			mpfr_sub(t1, t1, t3, MPFR_RNDN);
			mpfr_mul(t3, Cx, Ay, MPFR_RNDN);
			mpfr_mul(t4, Cy, Ax, MPFR_RNDN);
			mpfr_add(t3, t3, t4, MPFR_RNDN);
			mpfr_sub(t2, t2, t3, MPFR_RNDN);
			// (t3, t4) = (B-1)E - CD
			mpfr_sub_ui(t3, Bx, 1, MPFR_RNDN);
			mpfr_mul(t4, t3, Ex, MPFR_RNDN);
			mpfr_mul(t5, By, Ey, MPFR_RNDN);
			mpfr_sub(t4, t4, t5, MPFR_RNDN);
			mpfr_mul(t5, t3, Ey, MPFR_RNDN);
			mpfr_mul(t6, By, Ex, MPFR_RNDN);
			mpfr_sub(t5, t5, t6, MPFR_RNDN);
			mpfr_mul(t6, Cx, Dx, MPFR_RNDN);
			mpfr_mul(t7, Cy, Dy, MPFR_RNDN);
			mpfr_sub(t6, t6, t7, MPFR_RNDN);
			mpfr_sub(t3, t4, t6, MPFR_RNDN);
			mpfr_mul(t6, Cx, Dy, MPFR_RNDN);
			mpfr_mul(t7, Cy, Dx, MPFR_RNDN);
			mpfr_add(t6, t6, t7, MPFR_RNDN);
			mpfr_sub(t4, t5, t6, MPFR_RNDN);
			// (t3, t4) = 1/(t3, t4)  ; z^-1 = z* / (z z*)
			mpfr_sqr(t5, t3, MPFR_RNDN);
			mpfr_sqr(t6, t4, MPFR_RNDN);
			mpfr_add(t5, t5, t6, MPFR_RNDN);
			mpfr_div(t3, t3, t5, MPFR_RNDN);
			mpfr_div(t4, t4, t5, MPFR_RNDN);
			mpfr_neg(t4, t4, MPFR_RNDN);
			// (t1, t2) *= (t3, t4)
			mpfr_mul(t5, t1, t3, MPFR_RNDN);
			mpfr_mul(t6, t2, t4, MPFR_RNDN);
			mpfr_mul(t7, t1, t4, MPFR_RNDN);
			mpfr_mul(t8, t2, t3, MPFR_RNDN);
			mpfr_sub(t1, t5, t6, MPFR_RNDN);
			mpfr_add(t2, t7, t8, MPFR_RNDN);

			// (t1, t2) = b - (t1, t2)
			mpfr_sub(t1, bx, t1, MPFR_RNDN);
			mpfr_sub(t2, by, t2, MPFR_RNDN);

			mpfr_set(t01, t1, MPFR_RNDN);
			mpfr_set(t02, t2, MPFR_RNDN);
			mpfr_set(t03, bx, MPFR_RNDN);
			mpfr_set(t04, by, MPFR_RNDN);
			mpfr_sub(t01, t01, t03, MPFR_RNDN);
			mpfr_sub(t02, t02, t04, MPFR_RNDN);
			mpfr_add_ui(t01, t01, 1, MPFR_RNDN);
			mpfr_sub_ui(t01, t01, 1, MPFR_RNDN);
			mpfr_add_ui(t02, t02, 1, MPFR_RNDN);
			mpfr_sub_ui(t02, t02, 1, MPFR_RNDN);
			b_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);

			// (t5, t6) = E (A-w)
			mpfr_mul(t5, Ex, Ax, MPFR_RNDN);
			mpfr_mul(t6, Ey, Ay, MPFR_RNDN);
			mpfr_sub(t5, t5, t6, MPFR_RNDN);
			mpfr_mul(t6, Ex, Ay, MPFR_RNDN);
			mpfr_mul(t7, Ey, Ax, MPFR_RNDN);
			mpfr_add(t6, t6, t7, MPFR_RNDN);
			// B += 1
			mpfr_add_ui(Bx, Bx, 1, MPFR_RNDN);
			// (t7, t8) = D (B+1)
			mpfr_mul(t7, Dx, Bx, MPFR_RNDN);
			mpfr_mul(t8, Dy, By, MPFR_RNDN);
			mpfr_sub(t7, t7, t8, MPFR_RNDN);
			mpfr_mul(t8, Dx, By, MPFR_RNDN);
			mpfr_mul(t9, Dy, Bx, MPFR_RNDN);
			mpfr_add(t8, t8, t9, MPFR_RNDN);
			// (t5, t6) -= (t7, t8)
			mpfr_sub(t5, t5, t7, MPFR_RNDN);
			mpfr_sub(t6, t6, t8, MPFR_RNDN);
			// (t7, t8) = (t5, t6) * (t3, t4)
			mpfr_mul(t7, t3, t5, MPFR_RNDN);
			mpfr_mul(t8, t4, t6, MPFR_RNDN);
			mpfr_sub(t7, t7, t8, MPFR_RNDN);
			mpfr_mul(t8, t3, t6, MPFR_RNDN);
			mpfr_mul(t9, t4, t5, MPFR_RNDN);
			mpfr_add(t8, t8, t9, MPFR_RNDN);

			// (t3, t4) = w - (t7, t8)
			mpfr_sub(t3, wx, t7, MPFR_RNDN);
			mpfr_sub(t4, wy, t8, MPFR_RNDN);

			mpfr_set(t01, t3, MPFR_RNDN);
			mpfr_set(t02, t4, MPFR_RNDN);
			mpfr_set(t03, wx, MPFR_RNDN);
			mpfr_set(t04, wy, MPFR_RNDN);
			mpfr_sub(t01, t01, t03, MPFR_RNDN);
			mpfr_sub(t02, t02, t04, MPFR_RNDN);
			mpfr_add_ui(t01, t01, 1, MPFR_RNDN);
			mpfr_sub_ui(t01, t01, 1, MPFR_RNDN);
			mpfr_add_ui(t02, t02, 1, MPFR_RNDN);
			mpfr_sub_ui(t02, t02, 1, MPFR_RNDN);
			w_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);

			mpfr_set(bx, t1, MPFR_RNDN);
			mpfr_set(by, t2, MPFR_RNDN);
			mpfr_set(wx, t3, MPFR_RNDN);
			mpfr_set(wy, t4, MPFR_RNDN);
		} while (!(w_ok && b_ok) && --limit);
		if (! limit) goto cleanup;
		// t1 = |n - b|
		mpfr_sub(t1, nx, bx, MPFR_RNDN);
		mpfr_sqr(t1, t1, MPFR_RNDN);
		mpfr_sub(t2, ny, by, MPFR_RNDN);
		mpfr_sqr(t2, t2, MPFR_RNDN);
		mpfr_add(t1, t1, t2, MPFR_RNDN);
		mpfr_sqrt(t1, t1, MPFR_RNDN);
		bits = 2 * bits;
		mpfr_prec_round(nx, bits, MPFR_RNDN);
		mpfr_prec_round(ny, bits, MPFR_RNDN);
		mpfr_prec_round(wx, bits, MPFR_RNDN);
		mpfr_prec_round(wy, bits, MPFR_RNDN);
		mpfr_prec_round(bx, bits, MPFR_RNDN);
		mpfr_prec_round(by, bits, MPFR_RNDN);
		mpfr_set(zz, t1, MPFR_RNDN);
		mpfr_set_prec(Ax, bits);
		mpfr_set_prec(Ay, bits);
		mpfr_set_prec(Bx, bits);
		mpfr_set_prec(By, bits);
		mpfr_set_prec(Cx, bits);
		mpfr_set_prec(Cy, bits);
		mpfr_set_prec(Dx, bits);
		mpfr_set_prec(Dy, bits);
		mpfr_set_prec(Ex, bits);
		mpfr_set_prec(Ey, bits);
		mpfr_set_prec(t1, bits);
		mpfr_set_prec(t2, bits);
		mpfr_set_prec(t3, bits);
		mpfr_set_prec(t4, bits);
		mpfr_set_prec(t5, bits);
		mpfr_set_prec(t6, bits);
		mpfr_set_prec(t7, bits);
		mpfr_set_prec(t8, bits);
		mpfr_set_prec(t9, bits);
	} while (mpfr_zero_p(zz) || bits + mpfr_get_exp(zz) < mpfr_get_prec(zz) + accuracy);

	// copy to output
	r = 1;
	bits = mpfr_get_prec(nx);
	mpfr_set_prec(x, bits);
	mpfr_set_prec(y, bits);
	mpfr_set(x, nx, MPFR_RNDN);
	mpfr_set(y, ny, MPFR_RNDN);
	mpfr_set(z, zz, MPFR_RNDN);
cleanup:
	mpfr_clears(VARS, (mpfr_ptr) 0);
	#undef VARS
	return r;
}

int find_atom(atom_t *atom, const mpc_t c, const mpfr_t r, const unsigned int period)
{
	mpfr_t x;
	mpfr_t y;
	mpfr_t z;
	mpfr_inits2(mpfr_get_prec(mpc_realref(c)), x, y, (mpfr_ptr) 0);
	mpfr_inits2(mpfr_get_prec(r), z, (mpfr_ptr) 0);
	
	mpfr_set(x, mpc_realref(c), MPFR_RNDN);
	mpfr_set(y, mpc_imagref(c), MPFR_RNDN);
	mpfr_set(z, r, MPFR_RNDN);
	
	int ok = muatom(period, x, y, z);
	if (ok)
	{
		const mpfr_prec_t prec = (mpfr_prec_t)(ceil(log10(2.0) * mpfr_get_prec(x)));
		
		mpc_init2(atom->nucleus, prec);
		mpfr_init2(atom->radius, mpfr_get_prec(x));
		
		mpc_set_fr_fr(atom->nucleus, x, y, MPFR_RNDN);
		mpfr_set(atom->radius, x, MPFR_RNDN);
		atom->period = period;
	}
	
	mpfr_clears(x, y, z, (mpfr_ptr) 0);
	
	return ok;
}