#ifndef GLITCHANALYZER_H
#define GLITCHANALYZER_H

#include <stdatomic.h>
#include <stdint.h>
#include <CL/cl.h>

typedef struct
{
	unsigned int width;
	unsigned int height;
	
	cl_context context;
	cl_device_id deviceID;
	cl_command_queue commandQueue;
	
	cl_program program;
	
	cl_kernel erodeKernel;
	size_t erodeKernelSize;
	
	cl_mem glitchImageBuffer[2];
	cl_mem infoBuffer;
	
	size_t glitchImageSize;
	unsigned char *glitchImage;
} glitchanalyzer_t;

void glitchanalyzer_init(glitchanalyzer_t *g, const cl_command_queue commandQueue, const unsigned int width, const unsigned int height);
void glitchanalyzer_destroy(glitchanalyzer_t *g);

void glitchanalyzer_clear(glitchanalyzer_t *g);
void glitchanalyzer_find_blob(glitchanalyzer_t *g, unsigned int *x, unsigned int *y, atomic_bool *stop);

static inline void glitchanalyzer_set(glitchanalyzer_t *g, const unsigned int x, const unsigned int y)
{
	//g->glitchImage[(y * g->width + x) >> 5] |= (1u << (offset & 31));
	g->glitchImage[y * g->width + x] = 1;
}

#endif

