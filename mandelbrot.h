#ifndef MANDELBROT_H
#define MANDELBROT_H

#include <stdint.h>
#include <stdatomic.h>
#include <pthread.h>
#include <semaphore.h>
#include <mpfr.h>
#include <mpc.h>
#include <CL/cl.h>

#include "clhelper.h"
#include "referencepoint.h"
#include "glitchanalyzer.h"

#define ESCAPED_BIT (1u << 31)
#define GLITCHED (0xFFFFFFFFu)

typedef void (*mandelbrot_processor_func_t)(void *arg);
typedef struct mandelbrot_processor mandelbrot_processor_t;

typedef struct chunk chunk_t;
typedef struct mandelbrot mandelbrot_t;
typedef struct position position_t;

typedef void (*mandelbrot_update_callback_t)(mandelbrot_t *m, const position_t *positions, const unsigned int count, void *arg);

struct position
{
	uint16_t x;
	uint16_t y;
};

struct chunk
{
	unsigned int count;
	const position_t *positions;
	uint32_t *iterations;

	chunk_t *next;
};

struct mandelbrot
{
	unsigned int maxIterations;
	unsigned int numCoefficients;
	unsigned int width;
	unsigned int height;

	uint32_t *iterations;
	position_t *positions;
	position_t *positionsNext;

	uint32_t *colors;
	const unsigned char *gradient;
	unsigned int gradientSize;

	mpfr_prec_t precision;
	mpc_t center;
	mpfr_t radius;

	bool findGoodReference;
	referencepoint_t *referencePoint;

	unsigned int numProcessors;
	mandelbrot_processor_t *processors;

	pthread_mutex_t chunkQueueLock;
	chunk_t *firstChunk;
	chunk_t *lastChunk;

	mandelbrot_update_callback_t updateCallback;
	void *updateParameter;

	pthread_t thread;
	sem_t started;
	atomic_bool stop;

	unsigned int count;
	unsigned int processedPixels;
	unsigned int numGlitches;
	atomic_uint offset;

	glitchanalyzer_t *glitchAnalyzer;
};

void mandelbrot_init(mandelbrot_t *m, const unsigned int numCoefficients, const unsigned int width, const unsigned int height);
void mandelbrot_glitch_init(mandelbrot_t *m, const cl_command_queue commandQueue);
void mandelbrot_destroy(mandelbrot_t *m);

mandelbrot_processor_t *mandelbrot_add_processor(mandelbrot_t *m, void *data, const mandelbrot_processor_func_t compute, const mandelbrot_processor_func_t destroy);
int mandelbrot_remove_processor(mandelbrot_t *m, mandelbrot_processor_t *processor);

chunk_t *mandelbrot_request_chunk(mandelbrot_t *m, const unsigned int maxCount);
void mandelbrot_submit_chunk(mandelbrot_t *m, chunk_t *chunk);

unsigned int mandelbrot_get_max_iterations(const mandelbrot_t *m);
void mandelbrot_set_max_iterations(mandelbrot_t *m, const unsigned int maxIterations);
unsigned int mandelbrot_get_number_of_coefficients(const mandelbrot_t *m);
unsigned int mandelbrot_get_width(const mandelbrot_t *m);
unsigned int mandelbrot_get_height(const mandelbrot_t *m);
void mandelbrot_set_size(mandelbrot_t *m, const unsigned int width, const unsigned int height);
const uint32_t *mandelbrot_get_iterations(const mandelbrot_t *m);
const uint32_t *mandelbrot_get_colors(const mandelbrot_t *m);
mpfr_prec_t mandelbrot_get_precision(const mandelbrot_t *m);
void mandelbrot_set_precision(mandelbrot_t *m, const mpfr_prec_t precision);
void mandelbrot_auto_precision(mandelbrot_t *m);
referencepoint_t *mandelbrot_get_reference_point(mandelbrot_t *m);
bool mandelbrot_get_use_approximation(const mandelbrot_t *m);
void mandelbrot_set_use_approximation(mandelbrot_t *m, const bool useApproximation);

void mandelbrot_from_string(mandelbrot_t *m, const char *real, const char *imag, const char *radius);
void mandelbrot_parse_sft(mandelbrot_t *m, const char *str);

void mandelbrot_reset(mandelbrot_t *m);
void mandelbrot_move(mandelbrot_t *m, const double x, const double y);
void mandelbrot_zoom(mandelbrot_t *m, const double x, const double y, const double zoomFactor);
void mandelbrot_compute(mandelbrot_t *m);

void mandelbrot_start(mandelbrot_t *m);
void mandelbrot_wait(mandelbrot_t *m);
void mandelbrot_stop(mandelbrot_t *m, bool wait);
bool mandelbrot_stop_requested(const mandelbrot_t *m);

#endif
