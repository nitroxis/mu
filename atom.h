#ifndef ATOM_H
#define ATOM_H

#include <mpfr.h>
#include <mpc.h>

typedef struct
{
	mpc_t nucleus;
	mpfr_t radius;
	unsigned int period;
} atom_t;

unsigned int find_period(const mpc_t c0, const mpfr_t r, const unsigned int maxIterations);
int find_atom(atom_t *atom, const mpc_t c, const mpfr_t r, const unsigned int period);

#endif